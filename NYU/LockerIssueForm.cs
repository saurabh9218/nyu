﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class LockerIssueForm : Form
    {
        private static Locker locker;
        private static Person person;

        public LockerIssueForm(Person p, Locker l)
        {
            InitializeComponent();
            locker = l;
            person = p;
            fillLockerCard();
        }

        private void fillLockerCard()
        {
            nyuidtext.Text = person.getNYUID();
            fnametext.Text = person.getFirstName();
            lnametext.Text = person.getLastName();
            emailtext.Text = person.getEmail();
            phonetext.Text = person.getPhone();
            gendertext.Text = person.getGender();

            leveltext.Text = locker.getLevel();
            numbertext.Text = locker.getNumber();
            serialtext.Text = locker.getSerial();
            string[] combo = locker.getCombo().Split('-');
            combo1text.Text = combo[0];
            combo2text.Text = combo[1];
            combo3text.Text = combo[2];
            sizetext.Text = locker.getSize();
        }

        private void donebtn_Click(object sender, EventArgs e)
        {
            List<string> parameters = new List<string>();

            parameters.Add(nyuidtext.Text);
            parameters.Add(fnametext.Text);
            parameters.Add(lnametext.Text);
            parameters.Add(emailtext.Text);
            parameters.Add(phonetext.Text);
            parameters.Add(gendertext.Text);
           
            parameters.Add(leveltext.Text);
            parameters.Add(numbertext.Text);
            parameters.Add(serialtext.Text);
            parameters.Add(combo1text.Text);
            parameters.Add(combo2text.Text);
            parameters.Add(combo3text.Text);
            parameters.Add(sizetext.Text);

            parameters.Add(issuedbytext.Text);
            parameters.Add(semtext.Text);

            if (Helper.isBlank(parameters))
                MessageBox.Show(Variables.FILL_DETAILS);

            else
            {
                string issueDate = DateTime.Now.ToString("yyyy-MM-dd");
                string expiryDate = datetext.Value.Date.ToString("yyyy-MM-dd");
                string type = locker.getLockerFor();
                string LOCKERID = locker.getLockerId();
                int rowsAffected = person.addLocker(LOCKERID,issueDate,expiryDate,parameters[14],parameters[13]);

                if (rowsAffected == -1)
                    MessageBox.Show(Variables.CREATE_FAILED);
                else
                    MessageBox.Show(Variables.CREATE_SUCCESS);
                this.Close();
            }
           
        }

        private void issuedbytext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }


    }
}
