﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class homeform : Form
    {
        public homeform()
        {
            InitializeComponent();
           
        }

     
        private void patroncenterbtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            PersonCenter form = new PersonCenter(Variables.ALL);
            form.Show();
        }

        private void lockercenterbtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            LockerListForm form = new LockerListForm(Variables.ALL);
            form.Show();
        }

        private void athletecenterbtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            PersonCenter form = new PersonCenter(Variables.ATHLETE);
            form.Show();
        }


        private void button5_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            TeamList form = new TeamList();
            form.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            EquipmentListForm form = new EquipmentListForm();
            form.Show();
        }

    }
}
