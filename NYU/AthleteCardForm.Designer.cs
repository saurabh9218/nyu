﻿namespace NYU
{
    partial class AthleteCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teamtext = new System.Windows.Forms.ComboBox();
            this.cardlabel = new System.Windows.Forms.Label();
            this.addbtn = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.phonetext = new System.Windows.Forms.TextBox();
            this.nyuidtext = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.gendertext = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.emailtext = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lnametext = new System.Windows.Forms.TextBox();
            this.fnametext = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.teamlabel = new System.Windows.Forms.Label();
            this.schoollabel = new System.Windows.Forms.Label();
            this.schooltext = new System.Windows.Forms.ComboBox();
            this.teamyrlabel = new System.Windows.Forms.Label();
            this.teamyeartext = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // teamtext
            // 
            this.teamtext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamtext.FormattingEnabled = true;
            this.teamtext.Location = new System.Drawing.Point(158, 65);
            this.teamtext.Name = "teamtext";
            this.teamtext.Size = new System.Drawing.Size(223, 23);
            this.teamtext.TabIndex = 0;
            // 
            // cardlabel
            // 
            this.cardlabel.AutoSize = true;
            this.cardlabel.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardlabel.ForeColor = System.Drawing.Color.White;
            this.cardlabel.Location = new System.Drawing.Point(268, 18);
            this.cardlabel.Name = "cardlabel";
            this.cardlabel.Size = new System.Drawing.Size(161, 29);
            this.cardlabel.TabIndex = 135;
            this.cardlabel.Text = "ATHLETE CARD";
            // 
            // addbtn
            // 
            this.addbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addbtn.Location = new System.Drawing.Point(306, 314);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(75, 29);
            this.addbtn.TabIndex = 134;
            this.addbtn.Text = "ADD";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.addbtn_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(379, 160);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 15);
            this.label15.TabIndex = 131;
            this.label15.Text = "Gender*:";
            // 
            // phonetext
            // 
            this.phonetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phonetext.Location = new System.Drawing.Point(480, 202);
            this.phonetext.Name = "phonetext";
            this.phonetext.Size = new System.Drawing.Size(116, 23);
            this.phonetext.TabIndex = 130;
            this.phonetext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phonetext_KeyPress);
            // 
            // nyuidtext
            // 
            this.nyuidtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nyuidtext.Location = new System.Drawing.Point(158, 205);
            this.nyuidtext.Name = "nyuidtext";
            this.nyuidtext.Size = new System.Drawing.Size(144, 23);
            this.nyuidtext.TabIndex = 129;
            this.nyuidtext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nyuidtext_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(380, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 128;
            this.label1.Text = "Telephone*:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(39, 208);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 15);
            this.label11.TabIndex = 127;
            this.label11.Text = "N#/Alumni ID*#:";
            // 
            // gendertext
            // 
            this.gendertext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gendertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gendertext.FormattingEnabled = true;
            this.gendertext.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.gendertext.Location = new System.Drawing.Point(480, 157);
            this.gendertext.Name = "gendertext";
            this.gendertext.Size = new System.Drawing.Size(86, 23);
            this.gendertext.TabIndex = 126;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(380, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 125;
            this.label7.Text = "Last Name*:";
            // 
            // emailtext
            // 
            this.emailtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailtext.Location = new System.Drawing.Point(159, 157);
            this.emailtext.Name = "emailtext";
            this.emailtext.Size = new System.Drawing.Size(144, 23);
            this.emailtext.TabIndex = 124;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(39, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 15);
            this.label9.TabIndex = 123;
            this.label9.Text = "Email*:";
            // 
            // lnametext
            // 
            this.lnametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnametext.Location = new System.Drawing.Point(480, 109);
            this.lnametext.Name = "lnametext";
            this.lnametext.Size = new System.Drawing.Size(116, 23);
            this.lnametext.TabIndex = 122;
            this.lnametext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lnametext_KeyPress);
            // 
            // fnametext
            // 
            this.fnametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnametext.Location = new System.Drawing.Point(159, 109);
            this.fnametext.Name = "fnametext";
            this.fnametext.Size = new System.Drawing.Size(144, 23);
            this.fnametext.TabIndex = 121;
            this.fnametext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fnametext_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(39, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 15);
            this.label8.TabIndex = 120;
            this.label8.Text = "First Name*: ";
            // 
            // teamlabel
            // 
            this.teamlabel.AutoSize = true;
            this.teamlabel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamlabel.ForeColor = System.Drawing.Color.White;
            this.teamlabel.Location = new System.Drawing.Point(39, 67);
            this.teamlabel.Name = "teamlabel";
            this.teamlabel.Size = new System.Drawing.Size(49, 15);
            this.teamlabel.TabIndex = 136;
            this.teamlabel.Text = "Team*: ";
            // 
            // schoollabel
            // 
            this.schoollabel.AutoSize = true;
            this.schoollabel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schoollabel.ForeColor = System.Drawing.Color.White;
            this.schoollabel.Location = new System.Drawing.Point(39, 258);
            this.schoollabel.Name = "schoollabel";
            this.schoollabel.Size = new System.Drawing.Size(79, 15);
            this.schoollabel.TabIndex = 138;
            this.schoollabel.Text = "School Year*:";
            // 
            // schooltext
            // 
            this.schooltext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.schooltext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schooltext.FormattingEnabled = true;
            this.schooltext.Items.AddRange(new object[] {
            "",
            "Freshman",
            "Junior",
            "Sophomore",
            "Senior"});
            this.schooltext.Location = new System.Drawing.Point(158, 255);
            this.schooltext.Name = "schooltext";
            this.schooltext.Size = new System.Drawing.Size(144, 23);
            this.schooltext.TabIndex = 137;
            // 
            // teamyrlabel
            // 
            this.teamyrlabel.AutoSize = true;
            this.teamyrlabel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamyrlabel.ForeColor = System.Drawing.Color.White;
            this.teamyrlabel.Location = new System.Drawing.Point(380, 257);
            this.teamyrlabel.Name = "teamyrlabel";
            this.teamyrlabel.Size = new System.Drawing.Size(73, 15);
            this.teamyrlabel.TabIndex = 140;
            this.teamyrlabel.Text = "Team Year*:";
            // 
            // teamyeartext
            // 
            this.teamyeartext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamyeartext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamyeartext.FormattingEnabled = true;
            this.teamyeartext.Items.AddRange(new object[] {
            "",
            "1st",
            "2nd",
            "3rd",
            "4th"});
            this.teamyeartext.Location = new System.Drawing.Point(480, 255);
            this.teamyeartext.Name = "teamyeartext";
            this.teamyeartext.Size = new System.Drawing.Size(86, 23);
            this.teamyeartext.TabIndex = 139;
            // 
            // AthleteCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(657, 377);
            this.Controls.Add(this.teamyrlabel);
            this.Controls.Add(this.teamyeartext);
            this.Controls.Add(this.schoollabel);
            this.Controls.Add(this.schooltext);
            this.Controls.Add(this.teamlabel);
            this.Controls.Add(this.cardlabel);
            this.Controls.Add(this.addbtn);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.phonetext);
            this.Controls.Add(this.nyuidtext);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.gendertext);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.emailtext);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lnametext);
            this.Controls.Add(this.fnametext);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.teamtext);
            this.Name = "AthleteCardForm";
            this.Text = "Athlete Card Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox teamtext;
        private System.Windows.Forms.Label cardlabel;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox phonetext;
        private System.Windows.Forms.TextBox nyuidtext;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox gendertext;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox emailtext;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox lnametext;
        private System.Windows.Forms.TextBox fnametext;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label teamlabel;
        private System.Windows.Forms.Label schoollabel;
        private System.Windows.Forms.ComboBox schooltext;
        private System.Windows.Forms.Label teamyrlabel;
        private System.Windows.Forms.ComboBox teamyeartext;
    }
}