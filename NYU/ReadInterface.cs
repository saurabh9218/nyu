﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYU
{
    interface ReadInterface
    {
        List<List<string>> readById(string ID);
    }
}
