﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYU
{
    class Athlete : Person
    {
        private string NYUID, SchoolYear, TeamYear, TEAMID;

        public Athlete(string NYUID, string FirstName, string LastName, string Email, string Phone,
            string Gender, string PersonType, string SchoolYear, string TeamYear, string TEAMID)
            : base(NYUID, FirstName, LastName, Email, Phone, Gender, PersonType)
        {
            this.NYUID = NYUID;
            this.SchoolYear = SchoolYear;
            this.TeamYear = TeamYear;
            this.TEAMID = TEAMID;
        }

        public Athlete()
        {
            // TODO: Complete member initialization
        }

        public override int create()
        {
            int rowsAffected = base.create();
            if (rowsAffected != -1)
            {
                string procedureName = Variables.CREATE_ATHLETE_PROCEDURE;
                return Database.IntegerCommonPoint(procedureName, NYUID, TEAMID, SchoolYear, TeamYear);
            }
            return -1;
        }

        public override DataSet read()
        {
            string procedureName = Variables.READ_ATHLETE_PROCEDURE;
            return Database.DataSetCommonPoint(procedureName, null);
        }

        public override DataSet read(string search)
        {
            string procedureName = Variables.READ_ATHLETE_SEARCH_PROCEDURE;
            return Database.DataSetCommonPoint(procedureName, "%"+search+"%");
        }
    }
}
