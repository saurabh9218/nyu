﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class PersonCenter : Form
    {
        private static DataSet ds;

        private static string type;
        private static string NYUID;
        private static string FirstName;
        private static string LastName;
        private static string Email;
        private static string Phone;
        private static string Gender;

        public PersonCenter(string personType)
        {
            InitializeComponent();
            type = personType;
            LoadForm();
            LoadPersons();
        }

        private void LoadForm()
        {
            if (type.Equals(Variables.ALL))
            {
                issueeqbtn.Visible = false;
                addbtn.Text = "ADD A " + Variables.PERSON;
            }
            else if (type.Equals(Variables.ATHLETE))
                addbtn.Text = "ADD AN " + Variables.ATHLETE;
        }

        private void LoadPersons()
        {
            Person person = makePerson();
            ds = person.read();
            setDs();
        }

        private Person makePerson()
        {
            Person person = null;

            if (type.Equals(Variables.ATHLETE))
                person = new Athlete();
            else
                person = new Person();

            return person;
        }

        private void setDs()
        {
            if (ds != null)
            {
                personlist.DataSource = ds.Tables[0];
                personlist.Columns[0].Visible = type.Equals(Variables.ATHLETE) ? false : true;
            }
        }

        private void issuelockerbtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);

            fetchPerson();
            Person person = new Person(NYUID, FirstName, LastName, Email, Phone, Gender, type);
            int records = person.checkExists();

            if (records == 1)
            {
                DialogResult dialogResult = MessageBox.Show(Variables.CONTINUE, Variables.LOCKER_EXISTS, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    LockerListForm form = new LockerListForm(person);
                    form.Show();
                }
            }

            else
            {
                LockerListForm form = new LockerListForm(person);
                form.Show();
            }
        }

        private void fetchPerson()
        {
            int scale = 0;
            if (type.Equals(Variables.ATHLETE))
                scale = 1;
            else
                type = personlist.CurrentRow.Cells[6 + scale].Value.ToString();

            NYUID = personlist.CurrentRow.Cells[0 + scale].Value.ToString();
            FirstName = personlist.CurrentRow.Cells[1 + scale].Value.ToString();
            LastName = personlist.CurrentRow.Cells[2 + scale].Value.ToString();
            Email = personlist.CurrentRow.Cells[3 + scale].Value.ToString();
            Phone = personlist.CurrentRow.Cells[4 + scale].Value.ToString();
            Gender = personlist.CurrentRow.Cells[5 + scale].Value.ToString();
           
        }

        private void createbtn_Click(object sender, EventArgs e)
        {

        }

        private void editbtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            fetchPerson();
            Person person = new Person(NYUID, FirstName, LastName, Email, Phone, Gender, type);
            PersonCreateForm form = new PersonCreateForm(person);
            form.Show();
        }

        private void deletebtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            DialogResult dialogResult = MessageBox.Show(Variables.DELETE_MESSAGE, Variables.DELETE_TITLE, MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
                fetchPerson();
            Person person = new Person(NYUID, FirstName, LastName, Email, Phone, Gender, type);
            int rowsAffected = person.remove();

            if (rowsAffected == -1)
                MessageBox.Show(Variables.FAILED_DELETE);
            else
                MessageBox.Show(Variables.SUCCESS_DELETE);

            Close();
        }

        private void viewlockerbtn_Click(object sender, EventArgs e)
        {
            string NYUID = getCurrentNYUID();
            if (NYUID != null)
            {
                base.OnClick(e);
                LockerViewForm form = new LockerViewForm(NYUID, Variables.BY_PERSONID);
                form.Show();
            }

        }

        private string getCurrentNYUID()
        {
            int count = personlist.Rows.Count;
            int increment = 0;
            if (count > 1)
            {
                if (type.Equals(Variables.ATHLETE))
                    increment = 1;
                string NYUID = personlist.CurrentRow.Cells[0 + increment].Value.ToString();
                return NYUID;
            }
            return null;
        }
        private void addbtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            Form form = null;
            if (type.Equals(Variables.ATHLETE))
                form = new AthleteCardForm();
            else
                form = new PersonCreateForm();
            form.Show();
        }

        private void issueeqbtn_Click(object sender, EventArgs e)
        {
            string NYUID = getCurrentNYUID();
            if (NYUID != null)
            {
                base.OnClick(e);
                fetchPerson();
                Person person = new Person(NYUID, FirstName, LastName, Email, Phone, Gender, type);
                IssueReturnForm form = new IssueReturnForm(person);
                form.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadPersons();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string search = searchtxt.Text.ToString();
            Person person = makePerson();
            ds = person.read(search);
            setDs();
        }
    }
}
