﻿namespace NYU
{
    partial class LockerIssueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fnametext = new System.Windows.Forms.TextBox();
            this.lnametext = new System.Windows.Forms.TextBox();
            this.emailtext = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gendertext = new System.Windows.Forms.ComboBox();
            this.donebtn = new System.Windows.Forms.Button();
            this.issuedbytext = new System.Windows.Forms.TextBox();
            this.datetext = new System.Windows.Forms.DateTimePicker();
            this.sizetext = new System.Windows.Forms.TextBox();
            this.combo3text = new System.Windows.Forms.TextBox();
            this.combo2text = new System.Windows.Forms.TextBox();
            this.combo1text = new System.Windows.Forms.TextBox();
            this.serialtext = new System.Windows.Forms.TextBox();
            this.numbertext = new System.Windows.Forms.TextBox();
            this.leveltext = new System.Windows.Forms.ComboBox();
            this.phonetext = new System.Windows.Forms.TextBox();
            this.nyuidtext = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.semtext = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(41, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "First Name: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(275, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(214, 29);
            this.label6.TabIndex = 42;
            this.label6.Text = "LOCKER ISSUE CARD";
            // 
            // fnametext
            // 
            this.fnametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnametext.Location = new System.Drawing.Point(160, 105);
            this.fnametext.Name = "fnametext";
            this.fnametext.Size = new System.Drawing.Size(144, 23);
            this.fnametext.TabIndex = 43;
            // 
            // lnametext
            // 
            this.lnametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnametext.Location = new System.Drawing.Point(481, 105);
            this.lnametext.Name = "lnametext";
            this.lnametext.Size = new System.Drawing.Size(116, 23);
            this.lnametext.TabIndex = 44;
            // 
            // emailtext
            // 
            this.emailtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailtext.Location = new System.Drawing.Point(160, 153);
            this.emailtext.Name = "emailtext";
            this.emailtext.Size = new System.Drawing.Size(144, 23);
            this.emailtext.TabIndex = 67;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(43, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 15);
            this.label9.TabIndex = 56;
            this.label9.Text = "Email:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(381, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 15);
            this.label7.TabIndex = 79;
            this.label7.Text = "Last Name:";
            // 
            // gendertext
            // 
            this.gendertext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gendertext.FormattingEnabled = true;
            this.gendertext.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.gendertext.Location = new System.Drawing.Point(481, 153);
            this.gendertext.Name = "gendertext";
            this.gendertext.Size = new System.Drawing.Size(86, 23);
            this.gendertext.TabIndex = 80;
            // 
            // donebtn
            // 
            this.donebtn.Location = new System.Drawing.Point(322, 485);
            this.donebtn.Name = "donebtn";
            this.donebtn.Size = new System.Drawing.Size(87, 27);
            this.donebtn.TabIndex = 102;
            this.donebtn.Text = "DONE";
            this.donebtn.UseVisualStyleBackColor = true;
            this.donebtn.Click += new System.EventHandler(this.donebtn_Click);
            // 
            // issuedbytext
            // 
            this.issuedbytext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.issuedbytext.Location = new System.Drawing.Point(159, 436);
            this.issuedbytext.Name = "issuedbytext";
            this.issuedbytext.Size = new System.Drawing.Size(195, 23);
            this.issuedbytext.TabIndex = 101;
            this.issuedbytext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.issuedbytext_KeyPress);
            // 
            // datetext
            // 
            this.datetext.Location = new System.Drawing.Point(159, 385);
            this.datetext.Name = "datetext";
            this.datetext.Size = new System.Drawing.Size(231, 23);
            this.datetext.TabIndex = 100;
            // 
            // sizetext
            // 
            this.sizetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sizetext.Location = new System.Drawing.Point(159, 338);
            this.sizetext.Name = "sizetext";
            this.sizetext.Size = new System.Drawing.Size(86, 23);
            this.sizetext.TabIndex = 99;
            // 
            // combo3text
            // 
            this.combo3text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo3text.Location = new System.Drawing.Point(649, 291);
            this.combo3text.Name = "combo3text";
            this.combo3text.Size = new System.Drawing.Size(32, 23);
            this.combo3text.TabIndex = 98;
            // 
            // combo2text
            // 
            this.combo2text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo2text.Location = new System.Drawing.Point(609, 291);
            this.combo2text.Name = "combo2text";
            this.combo2text.Size = new System.Drawing.Size(32, 23);
            this.combo2text.TabIndex = 97;
            // 
            // combo1text
            // 
            this.combo1text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo1text.Location = new System.Drawing.Point(570, 291);
            this.combo1text.Name = "combo1text";
            this.combo1text.Size = new System.Drawing.Size(32, 23);
            this.combo1text.TabIndex = 96;
            // 
            // serialtext
            // 
            this.serialtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialtext.Location = new System.Drawing.Point(343, 291);
            this.serialtext.Name = "serialtext";
            this.serialtext.Size = new System.Drawing.Size(130, 23);
            this.serialtext.TabIndex = 95;
            // 
            // numbertext
            // 
            this.numbertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numbertext.Location = new System.Drawing.Point(159, 291);
            this.numbertext.Name = "numbertext";
            this.numbertext.Size = new System.Drawing.Size(84, 23);
            this.numbertext.TabIndex = 94;
            // 
            // leveltext
            // 
            this.leveltext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.leveltext.FormattingEnabled = true;
            this.leveltext.Items.AddRange(new object[] {
            "",
            "C-1",
            "C-2"});
            this.leveltext.Location = new System.Drawing.Point(159, 246);
            this.leveltext.Name = "leveltext";
            this.leveltext.Size = new System.Drawing.Size(86, 23);
            this.leveltext.TabIndex = 93;
            // 
            // phonetext
            // 
            this.phonetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phonetext.Location = new System.Drawing.Point(481, 198);
            this.phonetext.Name = "phonetext";
            this.phonetext.Size = new System.Drawing.Size(116, 23);
            this.phonetext.TabIndex = 92;
            // 
            // nyuidtext
            // 
            this.nyuidtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nyuidtext.Location = new System.Drawing.Point(159, 201);
            this.nyuidtext.Name = "nyuidtext";
            this.nyuidtext.Size = new System.Drawing.Size(144, 23);
            this.nyuidtext.TabIndex = 91;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(380, 438);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 15);
            this.label4.TabIndex = 90;
            this.label4.Text = "(Print Employee Name)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(40, 439);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 15);
            this.label10.TabIndex = 89;
            this.label10.Text = "Issued by:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(42, 341);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 15);
            this.label12.TabIndex = 88;
            this.label12.Text = "Size:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(40, 391);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 15);
            this.label13.TabIndex = 87;
            this.label13.Text = "Date Expires:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(42, 249);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 15);
            this.label14.TabIndex = 86;
            this.label14.Text = "Level:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(42, 294);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 83;
            this.label2.Text = "Locker#: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(276, 297);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 15);
            this.label3.TabIndex = 84;
            this.label3.Text = "Serial#: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(495, 297);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 85;
            this.label5.Text = "Combo: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(381, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 82;
            this.label1.Text = "Telephone:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(40, 204);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 15);
            this.label11.TabIndex = 81;
            this.label11.Text = "N#/Alumni ID#:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(380, 156);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 15);
            this.label15.TabIndex = 103;
            this.label15.Text = "Gender:";
            // 
            // semtext
            // 
            this.semtext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.semtext.FormattingEnabled = true;
            this.semtext.Items.AddRange(new object[] {
            "",
            "Spring",
            "Summer",
            "Fall"});
            this.semtext.Location = new System.Drawing.Point(570, 382);
            this.semtext.Name = "semtext";
            this.semtext.Size = new System.Drawing.Size(121, 23);
            this.semtext.TabIndex = 104;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(495, 390);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 15);
            this.label16.TabIndex = 105;
            this.label16.Text = "Semester:";
            // 
            // LockerIssueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(734, 531);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.semtext);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.donebtn);
            this.Controls.Add(this.issuedbytext);
            this.Controls.Add(this.datetext);
            this.Controls.Add(this.sizetext);
            this.Controls.Add(this.combo3text);
            this.Controls.Add(this.combo2text);
            this.Controls.Add(this.combo1text);
            this.Controls.Add(this.serialtext);
            this.Controls.Add(this.numbertext);
            this.Controls.Add(this.leveltext);
            this.Controls.Add(this.phonetext);
            this.Controls.Add(this.nyuidtext);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.gendertext);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.emailtext);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lnametext);
            this.Controls.Add(this.fnametext);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LockerIssueForm";
            this.Text = "LockerIssueForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox fnametext;
        private System.Windows.Forms.TextBox lnametext;
        private System.Windows.Forms.TextBox emailtext;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox gendertext;
        private System.Windows.Forms.Button donebtn;
        private System.Windows.Forms.TextBox issuedbytext;
        private System.Windows.Forms.DateTimePicker datetext;
        private System.Windows.Forms.TextBox sizetext;
        private System.Windows.Forms.TextBox combo3text;
        private System.Windows.Forms.TextBox combo2text;
        private System.Windows.Forms.TextBox combo1text;
        private System.Windows.Forms.TextBox serialtext;
        private System.Windows.Forms.TextBox numbertext;
        private System.Windows.Forms.ComboBox leveltext;
        private System.Windows.Forms.TextBox phonetext;
        private System.Windows.Forms.TextBox nyuidtext;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox semtext;
        private System.Windows.Forms.Label label16;
    }
}