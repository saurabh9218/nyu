﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class EquipmentForm : Form
    {
        private static string action;
        private static List<Team> teamList;
        private static Equipment equipment;

        public EquipmentForm()
        {

        }

        public EquipmentForm(string type)
        {
            InitializeComponent();
            action = type;
            LoadTeams();
        }

        public EquipmentForm(Equipment eq)
        {
            InitializeComponent();
            equipment = eq;
            action = Variables.EDIT;
            LoadForm();
        }

        private void LoadForm()
        {
            LoadTeams();
            nametext.Text = equipment.getEquipmentItem();
            typetext.Text = equipment.getType();
            styletext.Text = equipment.getStyle();
            gendertext.Text = equipment.getGender();
            sizetext.Text = equipment.getSize();
            colortext.Text = equipment.getColor();
            pricetext.Text = equipment.getPrice();
            statustext.Text = equipment.getStatus();
            string number = equipment.getQtyNum();
            datetext.Value = DateTime.Parse(equipment.getDate());
            string[] parts = number.Split('-');
            if (equipment.getType().Equals(Variables.NUMBERED))
            {
                if (parts.Length == 3 && parts[1] != null)
                    qtynumtext.Text = parts[1];
            }
            else
                qtynumtext.Text = number;
         }

        private void LoadTeams()
        {
            teamList = Team.getTeams();
            teamtext.DataSource = teamList;
            teamtext.ValueMember = "TEAMID";
            teamtext.DisplayMember = "Name";
            if (action.Equals(Variables.EDIT))
            {
                Team team = Team.getTeamById(equipment.getTeamID());
                teamtext.Text = team.Name;
            }
        }
        private void donebtn_Click(object sender, EventArgs e)
        {
            List<string> parameters = new List<string>();
            parameters.Add(teamtext.Text);//0
            parameters.Add(nametext.Text);//1
            parameters.Add(typetext.Text);//2
            string style = styletext.Text;
            string gender = gendertext.Text;
            string size = sizetext.Text;
            string color = colortext.Text;
            string price = pricetext.Value.ToString();
            parameters.Add(statustext.Text);//3
            parameters.Add(qtynumtext.Text);//4

            if (Helper.isBlank(parameters))
                MessageBox.Show(Variables.FILL_DETAILS);
            else
            {
                int rowsAffected = -1;
                string fail = "";
                string success = "";

                string number = "";
                if (parameters[2].Equals(Variables.NUMBERED))
                    number = size + "-" + parameters[4] + "-" + DateTime.Now.ToString("yy");
                else
                    number = parameters[4];
                string date = datetext.Value.Date.ToString("yyyy-MM-dd");
                string TEAMID = teamList[teamtext.SelectedIndex].TEAMID;

                if (action.Equals(Variables.ADD))
                {
                    fail = Variables.CREATE_FAILED;
                    success = Variables.CREATE_SUCCESS;
                    Equipment eq = new Equipment(null, TEAMID, parameters[1], parameters[2], number, style, gender, size, color, price, date, parameters[3]);
                    rowsAffected = eq.create();
                }

                else
                {
                    fail = Variables.UPDATE_FAILED;
                    success = Variables.UPDATE_SUCCESS;

                    equipment.setTeamId(TEAMID);
                    equipment.setEquipmentItem(parameters[1]);
                    equipment.setType(parameters[2]);
                    equipment.setStyle(style);
                    equipment.setGender(gender);
                    equipment.setSize(size);
                    equipment.setColor(color);
                    equipment.setPrice(price);
                    equipment.setQtyNum(number);
                    equipment.setDate(date);
                    equipment.setStatus(parameters[3]);
                    rowsAffected = equipment.update();

                }

                if (rowsAffected == -1)
                    MessageBox.Show(fail);
                else
                    MessageBox.Show(success);

                Close();
            }

        }

        private void nametext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }

        private void styletext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }

        private void colortext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }
    }
}
