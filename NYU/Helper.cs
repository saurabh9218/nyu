﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYU
{
    class Helper
    {
        public static bool isBlank(List<string>args)
        {
            for (int i = 0; i < args.Count; i++)
            {
                bool IsNullEmpty = String.IsNullOrEmpty(args[i]);
                bool IsNullWhiteSpace = String.IsNullOrWhiteSpace(args[i]);
                if (IsNullEmpty || IsNullWhiteSpace)
                    return true;
            }
                return false;
        }
    }
}
