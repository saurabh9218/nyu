﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class PersonCreateForm : Form
    {
        private static Person person;
        private static string action;

        public PersonCreateForm()
        {
            InitializeComponent();
            action = Variables.ADD;
        }

        public PersonCreateForm(Person p)
        {
            InitializeComponent();
            person = p;
            action = Variables.EDIT;
            LoadDetails();
        }

        private void LoadDetails()
        {
            typetext.Text = person.getPersonType();
            nyuidtext.Text = person.getNYUID();
            fnametext.Text = person.getFirstName();
            lnametext.Text = person.getLastName();
            emailtext.Text = person.getEmail();
            phonetext.Text = person.getPhone();
            gendertext.Text = person.getGender();
        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            List<string> parameters = new List<string>();

            parameters.Add(nyuidtext.Text);
            parameters.Add(fnametext.Text);
            parameters.Add(lnametext.Text);
            parameters.Add(emailtext.Text);
            parameters.Add(phonetext.Text);
            parameters.Add(gendertext.Text);
            parameters.Add(typetext.Text);

            if (!Regex.IsMatch(parameters[3],//email
                 @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                 RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)))
            {
                MessageBox.Show(Variables.WRONG_EMAIL);
                return;
            }

            if (Helper.isBlank(parameters))
                MessageBox.Show(Variables.FILL_DETAILS);
            else
            {
                int rowsAffected = -1;
                string fail = "";
                string success = "";
                if (action.Equals(Variables.ADD))
                {
                    fail = Variables.CREATE_FAILED;
                    success = Variables.CREATE_SUCCESS;
                    Person person = new Person(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], parameters[6]);
                    rowsAffected = person.create();
                }

                else if (action.Equals(Variables.EDIT))
                {
                    fail = Variables.UPDATE_FAILED;
                    success = Variables.UPDATE_SUCCESS;

                    string oldId = person.getNYUID();
                    person.setNYUID(parameters[0]);
                    person.setFirstName(parameters[1]);
                    person.setLastName(parameters[2]);
                    person.setEmail(parameters[3]);
                    person.setPhone(parameters[4]);
                    person.setGender(parameters[5]);
                    rowsAffected = person.update(oldId);
                }
                if (rowsAffected == -1)
                    MessageBox.Show(fail);
                else
                    MessageBox.Show(success);

                Close();
            }

        }

        private void fnametext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }

      
        private void nyuidtext_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != 8;
                
        }

        private void lnametext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }

        private void phonetext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9'))
                e.Handled = true;
        }
    }
}
