﻿namespace NYU
{
    partial class LockerViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fortext = new System.Windows.Forms.Label();
            this.sizetext = new System.Windows.Forms.Label();
            this.combotext = new System.Windows.Forms.Label();
            this.serialtext = new System.Windows.Forms.Label();
            this.leveltext = new System.Windows.Forms.Label();
            this.numbertext = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.issuedbytext = new System.Windows.Forms.Label();
            this.nametext = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.edatetext = new System.Windows.Forms.Label();
            this.idatetext = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.gendertext = new System.Windows.Forms.Label();
            this.phonetext = new System.Windows.Forms.Label();
            this.emailtext = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.semestertext = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.typetext = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.nyuidtext = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.prevbtn = new System.Windows.Forms.Button();
            this.nextbtn = new System.Windows.Forms.Button();
            this.removebtn = new System.Windows.Forms.Button();
            this.countfield = new System.Windows.Forms.Label();
            this.currentfield = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(140, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "LOCKER INFORMATION";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(30, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 15);
            this.label7.TabIndex = 44;
            this.label7.Text = "Locker For:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(30, 298);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 43;
            this.label8.Text = "Locker Size:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(264, 292);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 15);
            this.label4.TabIndex = 42;
            this.label4.Text = "Locker Combo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(264, 262);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 41;
            this.label3.Text = "Locker Serial:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(30, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 15);
            this.label2.TabIndex = 40;
            this.label2.Text = "Locker Level:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(264, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 39;
            this.label5.Text = "Locker #:";
            // 
            // fortext
            // 
            this.fortext.AutoSize = true;
            this.fortext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fortext.ForeColor = System.Drawing.Color.White;
            this.fortext.Location = new System.Drawing.Point(129, 228);
            this.fortext.Name = "fortext";
            this.fortext.Size = new System.Drawing.Size(25, 15);
            this.fortext.TabIndex = 50;
            this.fortext.Text = "For";
            // 
            // sizetext
            // 
            this.sizetext.AutoSize = true;
            this.sizetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sizetext.ForeColor = System.Drawing.Color.White;
            this.sizetext.Location = new System.Drawing.Point(129, 298);
            this.sizetext.Name = "sizetext";
            this.sizetext.Size = new System.Drawing.Size(28, 15);
            this.sizetext.TabIndex = 49;
            this.sizetext.Text = "Size";
            // 
            // combotext
            // 
            this.combotext.AutoSize = true;
            this.combotext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combotext.ForeColor = System.Drawing.Color.White;
            this.combotext.Location = new System.Drawing.Point(363, 292);
            this.combotext.Name = "combotext";
            this.combotext.Size = new System.Drawing.Size(46, 15);
            this.combotext.TabIndex = 48;
            this.combotext.Text = "Combo";
            // 
            // serialtext
            // 
            this.serialtext.AutoSize = true;
            this.serialtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialtext.ForeColor = System.Drawing.Color.White;
            this.serialtext.Location = new System.Drawing.Point(363, 262);
            this.serialtext.Name = "serialtext";
            this.serialtext.Size = new System.Drawing.Size(37, 15);
            this.serialtext.TabIndex = 47;
            this.serialtext.Text = "Serial";
            // 
            // leveltext
            // 
            this.leveltext.AutoSize = true;
            this.leveltext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leveltext.ForeColor = System.Drawing.Color.White;
            this.leveltext.Location = new System.Drawing.Point(129, 263);
            this.leveltext.Name = "leveltext";
            this.leveltext.Size = new System.Drawing.Size(36, 15);
            this.leveltext.TabIndex = 46;
            this.leveltext.Text = "Level";
            // 
            // numbertext
            // 
            this.numbertext.AutoSize = true;
            this.numbertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numbertext.ForeColor = System.Drawing.Color.White;
            this.numbertext.Location = new System.Drawing.Point(363, 231);
            this.numbertext.Name = "numbertext";
            this.numbertext.Size = new System.Drawing.Size(43, 15);
            this.numbertext.TabIndex = 45;
            this.numbertext.Text = "Locker";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(264, 323);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 15);
            this.label6.TabIndex = 51;
            this.label6.Text = "Issued By:";
            // 
            // issuedbytext
            // 
            this.issuedbytext.AutoSize = true;
            this.issuedbytext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.issuedbytext.ForeColor = System.Drawing.Color.White;
            this.issuedbytext.Location = new System.Drawing.Point(363, 323);
            this.issuedbytext.Name = "issuedbytext";
            this.issuedbytext.Size = new System.Drawing.Size(33, 15);
            this.issuedbytext.TabIndex = 52;
            this.issuedbytext.Text = "Date";
            // 
            // nametext
            // 
            this.nametext.AutoSize = true;
            this.nametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nametext.ForeColor = System.Drawing.Color.White;
            this.nametext.Location = new System.Drawing.Point(129, 89);
            this.nametext.Name = "nametext";
            this.nametext.Size = new System.Drawing.Size(44, 15);
            this.nametext.TabIndex = 58;
            this.nametext.Text = "Name:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(30, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 15);
            this.label10.TabIndex = 57;
            this.label10.Text = "Name:";
            // 
            // edatetext
            // 
            this.edatetext.AutoSize = true;
            this.edatetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edatetext.ForeColor = System.Drawing.Color.White;
            this.edatetext.Location = new System.Drawing.Point(129, 356);
            this.edatetext.Name = "edatetext";
            this.edatetext.Size = new System.Drawing.Size(33, 15);
            this.edatetext.TabIndex = 56;
            this.edatetext.Text = "Date";
            // 
            // idatetext
            // 
            this.idatetext.AutoSize = true;
            this.idatetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idatetext.ForeColor = System.Drawing.Color.White;
            this.idatetext.Location = new System.Drawing.Point(129, 326);
            this.idatetext.Name = "idatetext";
            this.idatetext.Size = new System.Drawing.Size(33, 15);
            this.idatetext.TabIndex = 55;
            this.idatetext.Text = "Date";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(30, 356);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 15);
            this.label13.TabIndex = 54;
            this.label13.Text = "Expiry Date:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(30, 326);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 53;
            this.label14.Text = "Issue Date:";
            // 
            // gendertext
            // 
            this.gendertext.AutoSize = true;
            this.gendertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gendertext.ForeColor = System.Drawing.Color.White;
            this.gendertext.Location = new System.Drawing.Point(129, 178);
            this.gendertext.Name = "gendertext";
            this.gendertext.Size = new System.Drawing.Size(48, 15);
            this.gendertext.TabIndex = 64;
            this.gendertext.Text = "Gender";
            // 
            // phonetext
            // 
            this.phonetext.AutoSize = true;
            this.phonetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phonetext.ForeColor = System.Drawing.Color.White;
            this.phonetext.Location = new System.Drawing.Point(129, 148);
            this.phonetext.Name = "phonetext";
            this.phonetext.Size = new System.Drawing.Size(42, 15);
            this.phonetext.TabIndex = 63;
            this.phonetext.Text = "Phone";
            // 
            // emailtext
            // 
            this.emailtext.AutoSize = true;
            this.emailtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailtext.ForeColor = System.Drawing.Color.White;
            this.emailtext.Location = new System.Drawing.Point(363, 148);
            this.emailtext.Name = "emailtext";
            this.emailtext.Size = new System.Drawing.Size(37, 15);
            this.emailtext.TabIndex = 62;
            this.emailtext.Text = "email";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(30, 178);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 15);
            this.label17.TabIndex = 61;
            this.label17.Text = "Gender:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(30, 148);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 15);
            this.label18.TabIndex = 60;
            this.label18.Text = "Phone:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(264, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 15);
            this.label19.TabIndex = 59;
            this.label19.Text = "Email:";
            // 
            // semestertext
            // 
            this.semestertext.AutoSize = true;
            this.semestertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.semestertext.ForeColor = System.Drawing.Color.White;
            this.semestertext.Location = new System.Drawing.Point(363, 356);
            this.semestertext.Name = "semestertext";
            this.semestertext.Size = new System.Drawing.Size(31, 15);
            this.semestertext.TabIndex = 66;
            this.semestertext.Text = "Sem";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(264, 356);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 15);
            this.label15.TabIndex = 65;
            this.label15.Text = "Semester:";
            // 
            // typetext
            // 
            this.typetext.AutoSize = true;
            this.typetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typetext.ForeColor = System.Drawing.Color.White;
            this.typetext.Location = new System.Drawing.Point(363, 89);
            this.typetext.Name = "typetext";
            this.typetext.Size = new System.Drawing.Size(32, 15);
            this.typetext.TabIndex = 68;
            this.typetext.Text = "type";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(264, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 15);
            this.label16.TabIndex = 67;
            this.label16.Text = "Person Type:";
            // 
            // nyuidtext
            // 
            this.nyuidtext.AutoSize = true;
            this.nyuidtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nyuidtext.ForeColor = System.Drawing.Color.White;
            this.nyuidtext.Location = new System.Drawing.Point(129, 119);
            this.nyuidtext.Name = "nyuidtext";
            this.nyuidtext.Size = new System.Drawing.Size(18, 15);
            this.nyuidtext.TabIndex = 70;
            this.nyuidtext.Text = "ID";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(30, 119);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 15);
            this.label20.TabIndex = 69;
            this.label20.Text = "NYUID:";
            // 
            // prevbtn
            // 
            this.prevbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prevbtn.Location = new System.Drawing.Point(93, 386);
            this.prevbtn.Name = "prevbtn";
            this.prevbtn.Size = new System.Drawing.Size(75, 30);
            this.prevbtn.TabIndex = 71;
            this.prevbtn.Text = "Prev";
            this.prevbtn.UseVisualStyleBackColor = true;
            this.prevbtn.Click += new System.EventHandler(this.prevbtn_Click);
            // 
            // nextbtn
            // 
            this.nextbtn.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.nextbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextbtn.Location = new System.Drawing.Point(174, 386);
            this.nextbtn.Name = "nextbtn";
            this.nextbtn.Size = new System.Drawing.Size(75, 30);
            this.nextbtn.TabIndex = 72;
            this.nextbtn.Text = "Next";
            this.nextbtn.UseVisualStyleBackColor = true;
            this.nextbtn.Click += new System.EventHandler(this.nextbtn_Click);
            // 
            // removebtn
            // 
            this.removebtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removebtn.Location = new System.Drawing.Point(256, 386);
            this.removebtn.Name = "removebtn";
            this.removebtn.Size = new System.Drawing.Size(75, 30);
            this.removebtn.TabIndex = 73;
            this.removebtn.Text = "Remove";
            this.removebtn.UseVisualStyleBackColor = true;
            this.removebtn.Click += new System.EventHandler(this.removebtn_Click);
            // 
            // countfield
            // 
            this.countfield.AutoSize = true;
            this.countfield.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countfield.ForeColor = System.Drawing.Color.White;
            this.countfield.Location = new System.Drawing.Point(134, 48);
            this.countfield.Name = "countfield";
            this.countfield.Size = new System.Drawing.Size(197, 23);
            this.countfield.TabIndex = 74;
            this.countfield.Text = "Total # locker(s) issued: ";
            // 
            // currentfield
            // 
            this.currentfield.AutoSize = true;
            this.currentfield.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentfield.ForeColor = System.Drawing.Color.White;
            this.currentfield.Location = new System.Drawing.Point(159, 201);
            this.currentfield.Name = "currentfield";
            this.currentfield.Size = new System.Drawing.Size(110, 15);
            this.currentfield.TabIndex = 75;
            this.currentfield.Text = "Currently Viewing:";
            // 
            // LockerViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(505, 426);
            this.Controls.Add(this.currentfield);
            this.Controls.Add(this.countfield);
            this.Controls.Add(this.removebtn);
            this.Controls.Add(this.nextbtn);
            this.Controls.Add(this.prevbtn);
            this.Controls.Add(this.nyuidtext);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.typetext);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.semestertext);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.gendertext);
            this.Controls.Add(this.phonetext);
            this.Controls.Add(this.emailtext);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.nametext);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.edatetext);
            this.Controls.Add(this.idatetext);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.issuedbytext);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.fortext);
            this.Controls.Add(this.sizetext);
            this.Controls.Add(this.combotext);
            this.Controls.Add(this.serialtext);
            this.Controls.Add(this.leveltext);
            this.Controls.Add(this.numbertext);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "LockerViewForm";
            this.Text = "View Locker Information";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label fortext;
        private System.Windows.Forms.Label sizetext;
        private System.Windows.Forms.Label combotext;
        private System.Windows.Forms.Label serialtext;
        private System.Windows.Forms.Label leveltext;
        private System.Windows.Forms.Label numbertext;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label issuedbytext;
        private System.Windows.Forms.Label nametext;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label edatetext;
        private System.Windows.Forms.Label idatetext;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label gendertext;
        private System.Windows.Forms.Label phonetext;
        private System.Windows.Forms.Label emailtext;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label semestertext;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label typetext;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label nyuidtext;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button prevbtn;
        private System.Windows.Forms.Button nextbtn;
        private System.Windows.Forms.Button removebtn;
        private System.Windows.Forms.Label countfield;
        private System.Windows.Forms.Label currentfield;
    }
}