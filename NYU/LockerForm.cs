﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class LockerForm : Form
    {
        private static string action;
        private static Locker locker;
        private List<Team> teamList;

        public LockerForm()
        {
            InitializeComponent();
        }

        public LockerForm(string actionType)
        {
            InitializeComponent();
            action = actionType;
            actionlabel.Text = "NEW LOCKER FORM";
        }

        public LockerForm(Locker l)
        {
            InitializeComponent();
            actionlabel.Text = "EDIT LOCKER INFORMATION";
            action = Variables.EDIT;
            locker = l;
           // MessageBox.Show(locker.getTeamName() + " " + locker.getTeamID());
            LoadTeams();
            LoadFormDetails();
        }

        private void LoadTeams()
        {
            teamList = Team.getTeams();
            teamtext.DataSource = teamList;
            teamtext.ValueMember = "TEAMID";
            teamtext.DisplayMember = "Name";
            if (action.Equals(Variables.EDIT))
            {
                Team team = Team.getTeamById(locker.getTeamID());
                teamtext.Text = team.Name;
            }
        }

        private void LoadFormDetails()
        {
            leveltext.Text = locker.getLevel();
            numbertext.Text = locker.getNumber();
            serialtext.Text = locker.getSerial();
            string[] combo = locker.getCombo().Split('-');
            combo1text.Text = combo[0];
            combo2text.Text = combo[1];
            combo3text.Text = combo[2];
            sizetext.Text = locker.getSize();
            fortext.Text = locker.getLockerFor();
            teamtext.Text= locker.getTeamName();
        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            List<string> parameters = new List<string>();
            parameters.Add(leveltext.Text);
            parameters.Add(numbertext.Text);
            parameters.Add(serialtext.Text);
            parameters.Add(combo1text.Text);
            parameters.Add(combo2text.Text);
            parameters.Add(combo3text.Text);
            parameters.Add(sizetext.Text);
            parameters.Add(fortext.Text);
          
            if (Helper.isBlank(parameters))
                MessageBox.Show(Variables.FILL_DETAILS);
            else
            {
                string combo = parameters[3] + "-" + parameters[4] + "-" + parameters[5];
                int rowsAffected = -1;
                string fail = "";
                string success = "";
                string TEAMID = null;
                int index = -1;
                string type = fortext.Text.ToString();
                if (type.Equals(Variables.ATHLETE))
                {
                    index = teamtext.SelectedIndex;
                    TEAMID = teamList[index].TEAMID;
                }

                if (action.Equals(Variables.ADD))
                {
                    Locker locker = new Locker("", parameters[0], parameters[1], parameters[2], combo, parameters[6], parameters[7], Variables.YES,TEAMID);
                    fail = Variables.CREATE_FAILED;
                    success = Variables.CREATE_SUCCESS;
                    rowsAffected = locker.create();
                }
                else
                {
                    string LOCKERID = locker.getLockerId();

                    locker.setLevel(parameters[0]);
                    locker.setNumber(parameters[1]);
                    locker.setSerial(parameters[2]);
                    locker.setCombo(combo);
                    locker.setSize(parameters[6]);
                    locker.setLockerFor(parameters[7]);

                    fail = Variables.UPDATE_FAILED;
                    success = Variables.UPDATE_SUCCESS;
                    rowsAffected = locker.update();
                }

                if (rowsAffected == -1)
                    MessageBox.Show(fail);
                else
                    MessageBox.Show(success);

                Close();
            }
        }

        private void fortext_SelectedIndexChanged(object sender, EventArgs e)
        {
            string type = fortext.Text.ToString();

            if (type.Equals(Variables.ATHLETE))
            {
                setLabels(true);
                if (teamList == null)
                    LoadTeams();
            }
            else
                setLabels(false);

        }

        private void setLabels(bool flag)
        {
            teamlabel.Visible = flag;
            teamtext.Visible = flag;
        }

        private void numbertext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }

        private void serialtext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9'))
                e.Handled = true;
        }

        private void combo1text_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9'))
                e.Handled = true;
        }

        private void combo2text_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9'))
                e.Handled = true;
        }

        private void combo3text_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9'))
                e.Handled = true;
        }
    }
}
