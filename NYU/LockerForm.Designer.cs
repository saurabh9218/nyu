﻿namespace NYU
{
    partial class LockerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.leveltext = new System.Windows.Forms.ComboBox();
            this.numbertext = new System.Windows.Forms.TextBox();
            this.serialtext = new System.Windows.Forms.TextBox();
            this.combo1text = new System.Windows.Forms.TextBox();
            this.combo2text = new System.Windows.Forms.TextBox();
            this.addbtn = new System.Windows.Forms.Button();
            this.sizetext = new System.Windows.Forms.ComboBox();
            this.combo3text = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.actionlabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fortext = new System.Windows.Forms.ComboBox();
            this.teamtext = new System.Windows.Forms.ComboBox();
            this.teamlabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(123, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Level*:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(123, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number*:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(123, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Serial*:";
            // 
            // leveltext
            // 
            this.leveltext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.leveltext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leveltext.FormattingEnabled = true;
            this.leveltext.Items.AddRange(new object[] {
            "",
            "C-1",
            "C-2"});
            this.leveltext.Location = new System.Drawing.Point(204, 114);
            this.leveltext.Name = "leveltext";
            this.leveltext.Size = new System.Drawing.Size(121, 23);
            this.leveltext.TabIndex = 6;
            // 
            // numbertext
            // 
            this.numbertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numbertext.Location = new System.Drawing.Point(204, 154);
            this.numbertext.Name = "numbertext";
            this.numbertext.Size = new System.Drawing.Size(121, 23);
            this.numbertext.TabIndex = 7;
            this.numbertext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbertext_KeyPress);
            // 
            // serialtext
            // 
            this.serialtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialtext.Location = new System.Drawing.Point(204, 192);
            this.serialtext.Name = "serialtext";
            this.serialtext.Size = new System.Drawing.Size(121, 23);
            this.serialtext.TabIndex = 8;
            this.serialtext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.serialtext_KeyPress);
            // 
            // combo1text
            // 
            this.combo1text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo1text.Location = new System.Drawing.Point(205, 231);
            this.combo1text.Name = "combo1text";
            this.combo1text.Size = new System.Drawing.Size(30, 23);
            this.combo1text.TabIndex = 9;
            this.combo1text.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.combo1text_KeyPress);
            // 
            // combo2text
            // 
            this.combo2text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo2text.Location = new System.Drawing.Point(241, 231);
            this.combo2text.Name = "combo2text";
            this.combo2text.Size = new System.Drawing.Size(31, 23);
            this.combo2text.TabIndex = 10;
            this.combo2text.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.combo2text_KeyPress);
            // 
            // addbtn
            // 
            this.addbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addbtn.Location = new System.Drawing.Point(185, 418);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(75, 28);
            this.addbtn.TabIndex = 19;
            this.addbtn.Text = "DONE";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.addbtn_Click);
            // 
            // sizetext
            // 
            this.sizetext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sizetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sizetext.FormattingEnabled = true;
            this.sizetext.Items.AddRange(new object[] {
            "",
            "QUARTER",
            "HALF",
            "FULL"});
            this.sizetext.Location = new System.Drawing.Point(204, 275);
            this.sizetext.Name = "sizetext";
            this.sizetext.Size = new System.Drawing.Size(121, 23);
            this.sizetext.TabIndex = 17;
            // 
            // combo3text
            // 
            this.combo3text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo3text.Location = new System.Drawing.Point(278, 231);
            this.combo3text.Name = "combo3text";
            this.combo3text.Size = new System.Drawing.Size(32, 23);
            this.combo3text.TabIndex = 16;
            this.combo3text.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.combo3text_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(123, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 15);
            this.label5.TabIndex = 14;
            this.label5.Text = "Size*:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(123, 234);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 15);
            this.label4.TabIndex = 13;
            this.label4.Text = "Combo*:";
            // 
            // actionlabel
            // 
            this.actionlabel.AutoSize = true;
            this.actionlabel.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actionlabel.ForeColor = System.Drawing.Color.White;
            this.actionlabel.Location = new System.Drawing.Point(126, 53);
            this.actionlabel.Name = "actionlabel";
            this.actionlabel.Size = new System.Drawing.Size(213, 29);
            this.actionlabel.TabIndex = 20;
            this.actionlabel.Text = "NEW LOCKER FORM";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(123, 314);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "For*:";
            // 
            // fortext
            // 
            this.fortext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fortext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fortext.FormattingEnabled = true;
            this.fortext.Items.AddRange(new object[] {
            "",
            "ATHLETE",
            "PATRON",
            "COACH",
            "STAFF"});
            this.fortext.Location = new System.Drawing.Point(204, 311);
            this.fortext.Name = "fortext";
            this.fortext.Size = new System.Drawing.Size(121, 23);
            this.fortext.TabIndex = 18;
            this.fortext.SelectedIndexChanged += new System.EventHandler(this.fortext_SelectedIndexChanged);
            // 
            // teamtext
            // 
            this.teamtext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamtext.FormattingEnabled = true;
            this.teamtext.Location = new System.Drawing.Point(205, 350);
            this.teamtext.Name = "teamtext";
            this.teamtext.Size = new System.Drawing.Size(121, 23);
            this.teamtext.TabIndex = 22;
            this.teamtext.Visible = false;
            // 
            // teamlabel
            // 
            this.teamlabel.AutoSize = true;
            this.teamlabel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamlabel.ForeColor = System.Drawing.Color.White;
            this.teamlabel.Location = new System.Drawing.Point(124, 353);
            this.teamlabel.Name = "teamlabel";
            this.teamlabel.Size = new System.Drawing.Size(46, 15);
            this.teamlabel.TabIndex = 21;
            this.teamlabel.Text = "Team*:";
            this.teamlabel.Visible = false;
            // 
            // LockerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(467, 501);
            this.Controls.Add(this.teamtext);
            this.Controls.Add(this.teamlabel);
            this.Controls.Add(this.actionlabel);
            this.Controls.Add(this.addbtn);
            this.Controls.Add(this.fortext);
            this.Controls.Add(this.sizetext);
            this.Controls.Add(this.combo3text);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.combo2text);
            this.Controls.Add(this.combo1text);
            this.Controls.Add(this.serialtext);
            this.Controls.Add(this.numbertext);
            this.Controls.Add(this.leveltext);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "LockerForm";
            this.Text = "LockerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox leveltext;
        private System.Windows.Forms.TextBox numbertext;
        private System.Windows.Forms.TextBox serialtext;
        private System.Windows.Forms.TextBox combo1text;
        private System.Windows.Forms.TextBox combo2text;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.ComboBox sizetext;
        private System.Windows.Forms.TextBox combo3text;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label actionlabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox fortext;
        private System.Windows.Forms.ComboBox teamtext;
        private System.Windows.Forms.Label teamlabel;
    }
}