﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class TeamCreateForm : Form
    {
        private static string action;
        private static DataSet ds;
        private Team team;
        public TeamCreateForm(string act)
        {
            InitializeComponent();
            action = act;
            teamlabel.Text = action + " TEAM FORM";
            LocadCoaches();

        }

        public TeamCreateForm(Team team)
            : this(Variables.EDIT)
        {
            // TODO: Complete member initialization
            this.team = team;
            if (action.Equals(Variables.EDIT))
                LoadForm();
        }

        private void LoadForm()
        {
            nametext.Text = team.Name;
            gendertext.Text = team.Gender;
        }

        private void LocadCoaches()
        {
            ds = Person.getCoaches();
            if (ds != null)
            {
                coachlist.DataSource = ds.Tables[0];
                coachlist.Columns[6].Visible = false;
            }
        }

        private void donebtn_Click(object sender, EventArgs e)
        {
            string name = nametext.Text;
            string gender = gendertext.Text;
            int count = coachlist.RowCount;
            string COACHID = null;
            if (count > 1)
                COACHID = coachlist.CurrentRow.Cells[0].Value.ToString();
            List<string> parameters = new List<string>();
            parameters.Add(name);
            parameters.Add(gender);
            if (Helper.isBlank(parameters))
            {
                MessageBox.Show(Variables.FILL_DETAILS);
                return;
            }

            string success = "", failed = "";
            int affectedRows = -1;
            if (action.Equals(Variables.ADD))
            {
                success = Variables.CREATE_SUCCESS;
                failed = Variables.CREATE_FAILED;

                Team t = new Team(null, name, gender, COACHID);
                affectedRows = t.create();
            }

            else
            {
                success = Variables.UPDATE_SUCCESS;
                failed = Variables.UPDATE_FAILED;

                Team newTeam = new Team(team.TEAMID, parameters[0], parameters[1], COACHID);
                affectedRows = newTeam.update();
            }

            if (affectedRows == -1)
                MessageBox.Show(failed);
            else
                MessageBox.Show(success);
            Close();
        }

       
        private void nametext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }
    }
}
