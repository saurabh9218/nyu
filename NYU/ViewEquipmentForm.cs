﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;

namespace NYU
{
    public partial class ViewEquipmentForm : Form
    {
        private static string id;
        private static DataSet ds;
        private int count;
        public ViewEquipmentForm(string id1)
        {
            InitializeComponent();
            id = id1;
            LoadEquipments();
        }

        private void LoadEquipments()
        {
            /*
             * select a.NYUID, eq.EQID, t.TeamName, e.Equipment_Item as Item, case e.Type WHEN 'NUmbered' then e.Qty_Number ELSE eq.Qty_Number END as Qty_Number
             * eq.IssueDate, eq.ReturnDate, p.Firstname, p.Lastname, p.Email, p.Phone, e.type, e.Qty_Number 
             * from equipmentathlete eq NATURAL JOIN athlete a 
             * NATURAL JOIN person p JOIN equipment e on eq.EQID=e.EQID JOIN team t on t.TEAMID = e.TEAMID WHERE eq.EQID = id
             * */

            ds = Equipment.readEquipmentById(id);
            if (ds != null)
            {
                viewlist.DataSource = ds.Tables[0];
                viewlist.Columns[1].Visible = false;
                viewlist.Columns[3].Visible = false;
                count = viewlist.RowCount;
                if (count > 1)
                {
                    teamtext.Text = viewlist.Rows[0].Cells[2].Value.ToString();
                    itemtext.Text = viewlist.Rows[0].Cells[3].Value.ToString();
                    string type = viewlist.Rows[0].Cells[11].Value.ToString();
                    if (type.Equals(Variables.UN_NUMBERED))
                    {
                        intext.Text = "" + Database.GetCountCommonPoint("getRemainingCountById", id);
                        outtext.Text = "" + Database.GetCountCommonPoint("getCheckedOutCountById", id);
                    }
                }
            }
        }
    }
}
