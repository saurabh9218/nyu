﻿namespace NYU
{
    partial class EquipmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.teamtext = new System.Windows.Forms.ComboBox();
            this.nametext = new System.Windows.Forms.TextBox();
            this.styletext = new System.Windows.Forms.TextBox();
            this.gendertext = new System.Windows.Forms.ComboBox();
            this.sizetext = new System.Windows.Forms.ComboBox();
            this.colortext = new System.Windows.Forms.TextBox();
            this.pricetext = new System.Windows.Forms.NumericUpDown();
            this.datetext = new System.Windows.Forms.DateTimePicker();
            this.statustext = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.typetext = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.qtynumtext = new System.Windows.Forms.NumericUpDown();
            this.donebtn = new System.Windows.Forms.Button();
            this.formname = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pricetext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtynumtext)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(64, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Team*:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(64, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Equipment Name*:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(64, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Style:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(64, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Gender*:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(64, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Size:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(64, 325);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Color:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(64, 375);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Price:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(64, 425);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "First Used Date:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(64, 475);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Status*:";
            // 
            // teamtext
            // 
            this.teamtext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamtext.FormattingEnabled = true;
            this.teamtext.Location = new System.Drawing.Point(184, 72);
            this.teamtext.Name = "teamtext";
            this.teamtext.Size = new System.Drawing.Size(183, 23);
            this.teamtext.TabIndex = 9;
            // 
            // nametext
            // 
            this.nametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nametext.Location = new System.Drawing.Point(184, 118);
            this.nametext.Name = "nametext";
            this.nametext.Size = new System.Drawing.Size(183, 23);
            this.nametext.TabIndex = 10;
            this.nametext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nametext_KeyPress);
            // 
            // styletext
            // 
            this.styletext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.styletext.Location = new System.Drawing.Point(184, 168);
            this.styletext.Name = "styletext";
            this.styletext.Size = new System.Drawing.Size(183, 23);
            this.styletext.TabIndex = 11;
            this.styletext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.styletext_KeyPress);
            // 
            // gendertext
            // 
            this.gendertext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gendertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gendertext.FormattingEnabled = true;
            this.gendertext.Items.AddRange(new object[] {
            "",
            "General",
            "Men",
            "Women"});
            this.gendertext.Location = new System.Drawing.Point(184, 214);
            this.gendertext.Name = "gendertext";
            this.gendertext.Size = new System.Drawing.Size(121, 23);
            this.gendertext.TabIndex = 12;
            // 
            // sizetext
            // 
            this.sizetext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sizetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sizetext.FormattingEnabled = true;
            this.sizetext.Items.AddRange(new object[] {
            "",
            "XY",
            "XS",
            "SM",
            "MD",
            "LG",
            "XL",
            "XXL",
            "XXXL"});
            this.sizetext.Location = new System.Drawing.Point(184, 264);
            this.sizetext.Name = "sizetext";
            this.sizetext.Size = new System.Drawing.Size(121, 23);
            this.sizetext.TabIndex = 13;
            // 
            // colortext
            // 
            this.colortext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colortext.Location = new System.Drawing.Point(184, 317);
            this.colortext.Name = "colortext";
            this.colortext.Size = new System.Drawing.Size(121, 23);
            this.colortext.TabIndex = 14;
            this.colortext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.colortext_KeyPress);
            // 
            // pricetext
            // 
            this.pricetext.DecimalPlaces = 2;
            this.pricetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pricetext.Location = new System.Drawing.Point(184, 367);
            this.pricetext.Name = "pricetext";
            this.pricetext.Size = new System.Drawing.Size(74, 23);
            this.pricetext.TabIndex = 15;
            // 
            // datetext
            // 
            this.datetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datetext.Location = new System.Drawing.Point(184, 419);
            this.datetext.Name = "datetext";
            this.datetext.Size = new System.Drawing.Size(212, 23);
            this.datetext.TabIndex = 16;
            // 
            // statustext
            // 
            this.statustext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.statustext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statustext.FormattingEnabled = true;
            this.statustext.Items.AddRange(new object[] {
            "",
            "IN",
            "OUT"});
            this.statustext.Location = new System.Drawing.Point(184, 467);
            this.statustext.Name = "statustext";
            this.statustext.Size = new System.Drawing.Size(121, 23);
            this.statustext.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(490, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 15);
            this.label10.TabIndex = 18;
            this.label10.Text = "Type*:";
            // 
            // typetext
            // 
            this.typetext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typetext.FormattingEnabled = true;
            this.typetext.Items.AddRange(new object[] {
            "",
            "Numbered",
            "Un-Numbered"});
            this.typetext.Location = new System.Drawing.Point(613, 113);
            this.typetext.Name = "typetext";
            this.typetext.Size = new System.Drawing.Size(134, 23);
            this.typetext.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(490, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 15);
            this.label12.TabIndex = 21;
            this.label12.Text = "Quantity/Number*:";
            // 
            // qtynumtext
            // 
            this.qtynumtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtynumtext.Location = new System.Drawing.Point(613, 163);
            this.qtynumtext.Name = "qtynumtext";
            this.qtynumtext.Size = new System.Drawing.Size(74, 23);
            this.qtynumtext.TabIndex = 23;
            // 
            // donebtn
            // 
            this.donebtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donebtn.Location = new System.Drawing.Point(356, 511);
            this.donebtn.Name = "donebtn";
            this.donebtn.Size = new System.Drawing.Size(75, 34);
            this.donebtn.TabIndex = 24;
            this.donebtn.Text = "DONE";
            this.donebtn.UseVisualStyleBackColor = true;
            this.donebtn.Click += new System.EventHandler(this.donebtn_Click);
            // 
            // formname
            // 
            this.formname.AutoSize = true;
            this.formname.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formname.ForeColor = System.Drawing.Color.White;
            this.formname.Location = new System.Drawing.Point(319, 18);
            this.formname.Name = "formname";
            this.formname.Size = new System.Drawing.Size(204, 29);
            this.formname.TabIndex = 25;
            this.formname.Text = "EQUIPMENT FORM";
            // 
            // EquipmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.formname);
            this.Controls.Add(this.donebtn);
            this.Controls.Add(this.qtynumtext);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.typetext);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.statustext);
            this.Controls.Add(this.datetext);
            this.Controls.Add(this.pricetext);
            this.Controls.Add(this.colortext);
            this.Controls.Add(this.sizetext);
            this.Controls.Add(this.gendertext);
            this.Controls.Add(this.styletext);
            this.Controls.Add(this.nametext);
            this.Controls.Add(this.teamtext);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EquipmentForm";
            this.Text = "EquipmentForm";
            ((System.ComponentModel.ISupportInitialize)(this.pricetext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtynumtext)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox teamtext;
        private System.Windows.Forms.TextBox nametext;
        private System.Windows.Forms.TextBox styletext;
        private System.Windows.Forms.ComboBox gendertext;
        private System.Windows.Forms.ComboBox sizetext;
        private System.Windows.Forms.TextBox colortext;
        private System.Windows.Forms.NumericUpDown pricetext;
        private System.Windows.Forms.DateTimePicker datetext;
        private System.Windows.Forms.ComboBox statustext;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox typetext;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown qtynumtext;
        private System.Windows.Forms.Button donebtn;
        private System.Windows.Forms.Label formname;
    }
}