﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class AthleteCardForm : Form
    {

        private List<Team> teamList;

        public AthleteCardForm()
        {
            InitializeComponent();
            loadTeams();
        }

        private void loadTeams()
        {
            teamList = Team.getTeams();
            teamtext.DataSource = teamList;
            teamtext.ValueMember = "TEAMID";
            teamtext.DisplayMember = "NAME";

        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            List<string> parameters = new List<string>();

            parameters.Add(nyuidtext.Text);
            parameters.Add(fnametext.Text);
            parameters.Add(lnametext.Text);
            parameters.Add(emailtext.Text);
            parameters.Add(phonetext.Text);
            parameters.Add(gendertext.Text);
            parameters.Add(schooltext.Text);
            parameters.Add(teamyeartext.Text);

            if (!Regex.IsMatch(parameters[3],//email
                 @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                 RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)))
            {
                MessageBox.Show(Variables.WRONG_EMAIL);
                return;
            }
           
            int index = teamtext.SelectedIndex;
            string TEAMID = teamList[index].TEAMID;
            
            if (Helper.isBlank(parameters))
                MessageBox.Show(Variables.FILL_DETAILS);
            else
            {
                Person person = new Athlete(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], Variables.ATHLETE, parameters[6], parameters[7], TEAMID);
                int rowsAffected = person.create();
                if (rowsAffected == -1)
                    MessageBox.Show(Variables.CREATE_FAILED);
                else
                    MessageBox.Show(Variables.CREATE_SUCCESS);

                Close();
            }
        }

        private void fnametext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }

        private void nyuidtext_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != 8;

        }

        private void lnametext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar > 32 && e.KeyChar < 48)
                e.Handled = true;
        }

        private void phonetext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9'))
                e.Handled = true;
        }

    }
}
