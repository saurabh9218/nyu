﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYU
{
    public class Team
    {
        private string COACHID;

        public string Name { get; set; }

        public string TEAMID { get; set; }

        public string Gender { get; set; }

        public Team()
        {
            // TODO: Complete member initialization
        }

        public Team(string TEAMID, string name, string gender, string COACHID)
        {
            // TODO: Complete member initialization
            this.TEAMID = TEAMID;
            this.Name = name;
            this.Gender = gender;
            this.COACHID = COACHID;
        }


        public static List<Team> getTeams()
        {
            string procedureName = Variables.READ_TEAMS;
            List<string> list = Database.ClassObjectListCommonPoint(procedureName, null);
            List<Team> teams = createTeamList(list);
            return teams;
        }

        private static List<Team> createTeamList(List<string> list)
        {
            List<Team> teams = new List<Team>();
            for (int i = 0; i < list.Count; )
            {
                Team t = new Team();
                t.TEAMID = list[i++];
                t.Name = list[i++];
                teams.Add(t);
            }
            return teams;
        }

        public static Team getTeamById(string TEAMID)
        {
            string procedureName = Variables.READ_TEAM_BY_ID;
            List<string> list = Database.ClassObjectListCommonPoint(procedureName, TEAMID);
            List<Team> teams = createTeamList(list);
            return teams[0];
        }

        public static DataSet getAllTeams()
        {
            string procedureName = Variables.READ_ALL_TEAMS_PROCEDURE;
            return Database.DataSetCommonPoint(procedureName, null);
        }

        public int create()
        {
            string procedureName = Variables.CREATE_TEAM_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, Name, Gender, COACHID);
        }

        public int update()
        {
            string procedureName = Variables.UPDATE_TEAM_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, TEAMID, Name, Gender, COACHID);
        }

        public int delete()
        {
            string procedureName = Variables.DELETE_TEAM_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, TEAMID);
        }
    }
}
