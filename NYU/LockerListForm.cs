﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class LockerListForm : Form
    {
        private static DataSet ds;
        private static int rowNumber;
        private static string type;
        private static Person person;

        public LockerListForm(string personType)
        {
            InitializeComponent();
            type = personType;
            if (type.Equals(Variables.ALL))
            {
                issuebtn.Visible = false;
                deletebtn.Visible = true;
                editbtn.Visible = true;
                addlockerbtn.Visible = true;
            }
            populateLockers();
        }

        public LockerListForm(Person p)
        {
            // TODO: Complete member initialization
            person = p;
            InitializeComponent();
            type = person.getPersonType();
            populateLockers();
        }

        private void populateLockers()
        {
            Locker lockers = new Locker();
            if (type.Equals(Variables.ATHLETE))
                ds = lockers.read(type, person.getNYUID());
            else
                ds = lockers.read(type, null);
            if (ds != null)
            {
                lockerlist.DataSource = ds.Tables[0];
                lockerlist.Columns[0].Visible = false;
                lockerlist.Columns[9].Visible = false;
            }
        }

        private void lockerlist_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowNumber = e.RowIndex;
           // type = lockerlist.CurrentRow.Cells[6].Value.ToString();
        }

        private void editbtn_Click(object sender, EventArgs e)
        {
            int count = lockerlist.Rows.Count;
            if (count > 1)
            {
                string LOCKERID = lockerlist.CurrentRow.Cells[0].Value.ToString();
                string level = lockerlist.CurrentRow.Cells[1].Value.ToString();
                string number = lockerlist.CurrentRow.Cells[2].Value.ToString();
                string serial = lockerlist.CurrentRow.Cells[3].Value.ToString();
                string combo = lockerlist.CurrentRow.Cells[4].Value.ToString();
                string size = lockerlist.CurrentRow.Cells[5].Value.ToString();
                string lockerFor = lockerlist.CurrentRow.Cells[6].Value.ToString();
                string TeamName = lockerlist.CurrentRow.Cells[8].Value.ToString();
                string TEAMID = lockerlist.CurrentRow.Cells[9].Value.ToString();
                Locker locker = new Locker(LOCKERID, level, number, serial, combo, size, lockerFor, null, TEAMID);
                locker.setTeamName(TeamName);
                base.OnClick(e);
                // LockerEditForm form = new LockerEditForm(locker);
                LockerForm form = new LockerForm(locker);
                form.Show();
            }
        }

        private void deletebtn_Click(object sender, EventArgs e)
        {
            int count = lockerlist.Rows.Count;
            if (count > 1)
            {
                DialogResult dialogResult = MessageBox.Show(Variables.DELETE_MESSAGE, Variables.DELETE_TITLE, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string LOCKERID = lockerlist.CurrentRow.Cells[0].Value.ToString();
                    Locker locker = new Locker();
                    int rowsAffected = locker.remove(LOCKERID);
                    if (rowsAffected == -1)
                        MessageBox.Show(Variables.FAILED_DELETE);
                    else
                    {
                        MessageBox.Show(Variables.SUCCESS_DELETE);
                        populateLockers();
                    }
                }
            }

        }

        private void viewbtn_Click(object sender, EventArgs e)
        {
            int count = lockerlist.Rows.Count;
            if (count > 1)
            {
                string availability = lockerlist.CurrentRow.Cells[7].Value.ToString();
                if (availability.Equals(Variables.NO))
                {
                    string LOCKERID = lockerlist.CurrentRow.Cells[0].Value.ToString();
                    base.OnClick(e);
                    LockerViewForm form = new LockerViewForm(LOCKERID, Variables.BY_LOCKERID);
                    form.Show();

                }
            }
        }

        private void issuebtn_Click(object sender, EventArgs e)
        {
            if (type.Equals("%"))
            {
                MessageBox.Show("Please use athlete/patron center to issue lockers");//try to issue from here itself
                return;
            }

            int count = lockerlist.Rows.Count;
            if (count > 1)
            {
                string availability = lockerlist.CurrentRow.Cells[7].Value.ToString();

                if (availability.Equals(Variables.YES))
                {
                    string lockerType = lockerlist.CurrentRow.Cells[6].Value.ToString();
                    DialogResult dialogResult = MessageBox.Show("This is " + lockerType + " locker! Proceed?", Variables.LOCKER_ALERT, MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string LOCKERID = lockerlist.CurrentRow.Cells[0].Value.ToString();
                        string level = lockerlist.CurrentRow.Cells[1].Value.ToString();
                        string number = lockerlist.CurrentRow.Cells[2].Value.ToString();
                        string serial = lockerlist.CurrentRow.Cells[3].Value.ToString();
                        string combo = lockerlist.CurrentRow.Cells[4].Value.ToString();
                        string size = lockerlist.CurrentRow.Cells[5].Value.ToString();
                        string lockerFor = lockerlist.CurrentRow.Cells[6].Value.ToString();

                        Locker locker = new Locker(LOCKERID, level, number, serial, combo, size, lockerFor, Variables.NO, null);
                        base.OnClick(e);
                        LockerIssueForm form = new LockerIssueForm(person, locker);
                        form.Show();
                    }

                }

                else
                    MessageBox.Show(Variables.UNAVAILABLE);
            }
        }

        private void refreshbtn_Click(object sender, EventArgs e)
        {
            populateLockers();
        }

        private void addlockerbtn_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            LockerForm form = new LockerForm(Variables.ADD);
            form.Show();
        }
    }
}
