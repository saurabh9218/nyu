﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYU
{
    public class Locker : ReadInterface
    {
        private string LOCKERID, Level, Number, Serial, Combo, Size, LockerFor, Availibility, TEAMID, TeamName;

        public void setLevel(string l)
        {
            Level = l;
        }

        public void setNumber(string n)
        {
            Number = n;
        }

        public void setSerial(string ser)
        {
            Serial = ser;
        }

        public void setCombo(string c)
        {
            Combo = c;
        }

        public void setSize(string s)
        {
            Size = s;
        }

        public void setLockerFor(string lockerFor)
        {
            LockerFor = lockerFor;
        }

        public string getLockerId()
        {
            return LOCKERID;
        }

        public string getLevel()
        {
            return Level;
        }

        public string getNumber()
        {
            return Number;
        }

        public string getSerial()
        {
            return Serial;
        }

        public string getCombo()
        {
            return Combo;
        }

        public string getSize()
        {
            return Size;
        }

        public string getLockerFor()
        {
            return LockerFor;
        }

        public string getAvailibility()
        {
            return Availibility;
        }

        public string getTeamID()
        {
            return TEAMID;
        }

        public Locker(string LOCKERID, string Level, string Number, string Serial, string Combo, string Size, string LockerFor, string Availibility, string TEAMID)
        {
            this.LOCKERID = LOCKERID;
            this.Level = Level;
            this.Number = Number;
            this.Serial = Serial;
            this.Combo = Combo;
            this.Size = Size;
            this.LockerFor = LockerFor;
            this.Availibility = Availibility;
            this.TEAMID = TEAMID;
        }

        public Locker()
        {
            // TODO: Complete member initialization
        }

        public int create()
        {
            // check code here
            string procedureName = Variables.CREATE_LOCKER_PROCEDURE;
            int rowsAffected = Database.IntegerCommonPoint(procedureName, Level, Number, Serial, Combo, Size, LockerFor, Availibility);

            if (rowsAffected != -1 && LockerFor.Equals(Variables.ATHLETE))
            {
                string procedure = Variables.CREATE_TEAM_LOCKER_PROCEDURE;
                return Database.IntegerCommonPoint(procedure, TEAMID);
            }
            return rowsAffected;
        }

        public DataSet read(string type, string NYUID)
        {
            if (NYUID != null)
            {
                string procedureName = Variables.READ_ATHLETE_LOCKER_PROCEDURE;
                return Database.DataSetCommonPoint(procedureName, NYUID);
            }
            else
            {
                string procedureName = Variables.READ_LOCKER_PROCEDURE;
                return Database.DataSetCommonPoint(procedureName, type);
            }

        }

        public int update()
        {
            string procedureName = Variables.UPDATE_LOCKER_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, LOCKERID, Level, Number, Serial, Combo, Size, LockerFor);
        }

        public int remove(string LOCKERID)
        {
            string procedureName = Variables.REMOVE_LOCKER_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, LOCKERID);
        }

        public List<List<string>> readById(string LOCKERID)
        {
            string procedureName = Variables.READ_BY_ID_LOCKER_PROCEDURE;
            return Database.ListCommonPoint(procedureName, LOCKERID);
        }

        public static int removePersonLockerByLockerId(string LOCKERID)
        {
            string procedureName = Variables.REMOVE_PERSON_LOCKER_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, LOCKERID);
        }



        public void setTeamName(string TeamName)
        {
            this.TeamName = TeamName;
        }

        public string getTeamName()
        {
            return TeamName;
        }
    }
}
