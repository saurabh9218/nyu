﻿namespace NYU
{
    partial class PersonCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addbtn = new System.Windows.Forms.Button();
            this.personlabel = new System.Windows.Forms.Label();
            this.typetext = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.phonetext = new System.Windows.Forms.TextBox();
            this.nyuidtext = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.gendertext = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.emailtext = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lnametext = new System.Windows.Forms.TextBox();
            this.fnametext = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // addbtn
            // 
            this.addbtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addbtn.Location = new System.Drawing.Point(299, 282);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(83, 30);
            this.addbtn.TabIndex = 118;
            this.addbtn.Text = "DONE";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.addbtn_Click);
            // 
            // personlabel
            // 
            this.personlabel.AutoSize = true;
            this.personlabel.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.personlabel.ForeColor = System.Drawing.Color.White;
            this.personlabel.Location = new System.Drawing.Point(220, 22);
            this.personlabel.Name = "personlabel";
            this.personlabel.Size = new System.Drawing.Size(247, 29);
            this.personlabel.TabIndex = 119;
            this.personlabel.Text = "PATRON INFORMATION";
            // 
            // typetext
            // 
            this.typetext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typetext.FormattingEnabled = true;
            this.typetext.Items.AddRange(new object[] {
            "",
            "PATRON",
            "ATHLETE",
            "COACH",
            "STAFF"});
            this.typetext.Location = new System.Drawing.Point(177, 75);
            this.typetext.Name = "typetext";
            this.typetext.Size = new System.Drawing.Size(143, 23);
            this.typetext.TabIndex = 121;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(397, 172);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 15);
            this.label15.TabIndex = 133;
            this.label15.Text = "Gender*:";
            // 
            // phonetext
            // 
            this.phonetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phonetext.Location = new System.Drawing.Point(498, 214);
            this.phonetext.Name = "phonetext";
            this.phonetext.Size = new System.Drawing.Size(116, 23);
            this.phonetext.TabIndex = 132;
            this.phonetext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phonetext_KeyPress);
            // 
            // nyuidtext
            // 
            this.nyuidtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nyuidtext.Location = new System.Drawing.Point(176, 217);
            this.nyuidtext.Name = "nyuidtext";
            this.nyuidtext.Size = new System.Drawing.Size(144, 23);
            this.nyuidtext.TabIndex = 131;
            this.nyuidtext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nyuidtext_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(398, 217);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 15);
            this.label2.TabIndex = 130;
            this.label2.Text = "Telephone*:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(57, 220);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 15);
            this.label11.TabIndex = 129;
            this.label11.Text = "N#/Alumni ID# *:";
            // 
            // gendertext
            // 
            this.gendertext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gendertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gendertext.FormattingEnabled = true;
            this.gendertext.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.gendertext.Location = new System.Drawing.Point(498, 169);
            this.gendertext.Name = "gendertext";
            this.gendertext.Size = new System.Drawing.Size(86, 23);
            this.gendertext.TabIndex = 128;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(398, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 127;
            this.label7.Text = "Last Name*:";
            // 
            // emailtext
            // 
            this.emailtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailtext.Location = new System.Drawing.Point(177, 169);
            this.emailtext.Name = "emailtext";
            this.emailtext.Size = new System.Drawing.Size(144, 23);
            this.emailtext.TabIndex = 126;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(60, 172);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 15);
            this.label9.TabIndex = 125;
            this.label9.Text = "Email*:";
            // 
            // lnametext
            // 
            this.lnametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnametext.Location = new System.Drawing.Point(498, 121);
            this.lnametext.Name = "lnametext";
            this.lnametext.Size = new System.Drawing.Size(116, 23);
            this.lnametext.TabIndex = 124;
            this.lnametext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lnametext_KeyPress);
            // 
            // fnametext
            // 
            this.fnametext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnametext.Location = new System.Drawing.Point(177, 121);
            this.fnametext.Name = "fnametext";
            this.fnametext.Size = new System.Drawing.Size(144, 23);
            this.fnametext.TabIndex = 123;
            this.fnametext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fnametext_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(58, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 15);
            this.label8.TabIndex = 122;
            this.label8.Text = "First Name*: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(60, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 15);
            this.label1.TabIndex = 134;
            this.label1.Text = "Person Type*: ";
            // 
            // PersonCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(670, 340);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.phonetext);
            this.Controls.Add(this.nyuidtext);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.gendertext);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.emailtext);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lnametext);
            this.Controls.Add(this.fnametext);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.typetext);
            this.Controls.Add(this.personlabel);
            this.Controls.Add(this.addbtn);
            this.Name = "PersonCreateForm";
            this.Text = "Patron Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label personlabel;
        private System.Windows.Forms.ComboBox typetext;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox phonetext;
        private System.Windows.Forms.TextBox nyuidtext;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox gendertext;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox emailtext;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox lnametext;
        private System.Windows.Forms.TextBox fnametext;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
    }
}