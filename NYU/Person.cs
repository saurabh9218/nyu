﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public class Person :ReadInterface
    {
        private string NYUID, FirstName, LastName, Email, Phone, Gender, PersonType;

        public Person() { }

        public Person(string NYUID, string FirstName, string LastName, string Email, string Phone, string Gender, string PersonType)
        {
            this.NYUID = NYUID;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Email = Email;
            this.Phone = Phone;
            this.Gender = Gender;
            this.PersonType = PersonType;
        }

        public void setNYUID(string id)
        {
            NYUID = id;
        }

        public void setFirstName(string fname)
        {
            FirstName = fname;
        }

        public void setLastName(string lname)
        {
            LastName = lname;
        }

        public void setEmail(string em)
        {
            Email = em;
        }

        public void setPhone(string ph)
        {
            Phone = ph;
        }

        public void setGender(string gen)
        {
            Gender = gen;
        }

        public string getNYUID()
        {
            return NYUID;
        }

        public string getFirstName()
        {
            return FirstName;
        }

        public string getLastName()
        {
            return LastName;
        }

        public string getEmail()
        {
            return Email;
        }

        public string getPhone()
        {
            return Phone;
        }

        public string getGender()
        {
            return Gender;
        }

        public string getPersonType()
        {
            return PersonType;
        }

        public virtual int create()
        {
            string procedureName = Variables.CREATE_PERSON_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, NYUID, FirstName, LastName, Email, Phone, Gender, PersonType);
        }

        public virtual DataSet read()
        {
            string procedureName = Variables.READ_PERSON_PROCEDURE;
            return Database.DataSetCommonPoint(procedureName, null);
        }

        public virtual DataSet read(string search)
        {
            string procedureName = Variables.READ_PERSON_SEARCH_PROCEDURE;
            return Database.DataSetCommonPoint(procedureName, "%" + search + "%");
        }


        public int addLocker(string LOCKERID, string idate, string edate, string sem, string issuedBy)
        {
            string procedureName = Variables.CREATE_PERSON_LOCKER_PROCEDURE;
            
            int rowsAffected = Database.IntegerCommonPoint(procedureName, LOCKERID, NYUID, idate, edate, sem, issuedBy);
            if (rowsAffected!=-1)
            {
                string name = Variables.UPDATE_LOCKER_AVAILABILITY_PROCEDURE;
                return Database.IntegerCommonPoint(name, "NO", LOCKERID);
            }
                return -1;
        }

        public int update(string oldId)
        {
            string procedureName = Variables.UPDATE_PERSON_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, NYUID, FirstName, LastName, Email, Phone, Gender,oldId);
        }

        public int checkExists()
        {
            string procedureName = Variables.EXISTS_PERSON_RECORD;
            return Database.GetCountCommonPoint(procedureName, NYUID);
        }

        public int remove()
        {
            string procedureName = Variables.REMOVE_PERSON_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, NYUID);
        }

        public List<List<string>> readById(string ID)
        {
            string procedureName = Variables.READ_BY_ID_PERSON_PROCEDURE;
            return Database.ListCommonPoint(procedureName, ID);
        }

        public static DataSet getCoaches()
        {
            string procedureName = Variables.READ_PERSON_BY_TYPE_PROCEDURE;
            return Database.DataSetCommonPoint(procedureName, Variables.COACH);
        }
    }
}
