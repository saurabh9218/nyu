﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYU
{
    public class Equipment
    {
        private string EQID, TEAMID, Equipment_Item, Type, Qty_Number, Style, Gender, Size, Color, Price, Date, ReturnedStatus;

        public Equipment(string EQID, string TEAMID, string Name, string type, string Qty_Number, string Style, string Gender, string size, string Color, string Price, string Date, string Status)
        {
            this.EQID = EQID;
            this.TEAMID = TEAMID;
            this.Equipment_Item = Name;
            this.Type = type;
            this.Qty_Number = Qty_Number;
            this.Style = Style;
            this.Gender = Gender;
            this.Size = size;
            this.Color = Color;
            this.Price = Price;
            this.Date = Date;
            this.ReturnedStatus = Status;

        }

        public static DataSet read()
        {
            string procedureName = Variables.READ_EQUIPMENT_PROCEDURE;
            return Database.DataSetCommonPoint(procedureName);
        }

        public int create()
        {
            string procedureName = Variables.CREATE_EQUIPMENT_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, TEAMID, Equipment_Item, Type, Qty_Number, Style, Gender, Size, Color, Price, Date, ReturnedStatus);
        }

        public string getTeamID()
        {
            return TEAMID;
        }

        public string getEquipmentItem()
        {
            return Equipment_Item;
        }

        public string getType()
        {
            return Type;
        }

        public string getStyle()
        {
            return Style;
        }

        public string getGender()
        {
            return Gender;
        }

        public string getSize()
        {
            return Size;
        }

        public string getColor()
        {
            return Color;
        }

        public string getPrice()
        {
            return Price;
        }

        public string getStatus()
        {
            return ReturnedStatus;
        }

        public string getQtyNum()
        {
            return Qty_Number;
        }

        public string getDate()
        {
            return Date;
        }

        public void setEquipmentItem(string item)
        {
            Equipment_Item = item;
        }

        public void setType(string type)
        {
            this.Type = type;
        }

        public void setStyle(string style)
        {
            this.Style = style;
        }

        public void setGender(string gen)
        {
            Gender = gen;
        }

        public void setSize(string size)
        {
            Size = size;
        }

        public void setColor(string color)
        {
            Color = color;
        }

        public void setPrice(string price)
        {
            Price = price;
        }

        public void setStatus(string status)
        {
            ReturnedStatus = status;
        }

        public void setQtyNum(string number)
        {
            Qty_Number = number;
        }

        public void setDate(string date)
        {
            Date = date;
        }

        public void setTeamId(string teamId)
        {
            TEAMID = teamId;
        }

        public int update()
        {
            string procedureName = Variables.UPDATE_EQUIPMENT_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, EQID, TEAMID, Equipment_Item, Type, Qty_Number, Style, Gender, Size, Color, Price, Date, ReturnedStatus);
        }

        public static int remove(string EQID)
        {
            string procedureName = Variables.REMOVE_EQUIPMENT_PROCEDURE;
            return Database.IntegerCommonPoint(procedureName, EQID);
        }

        public static DataSet readEquipmentBySize(string size)
        {
            string procedureName = Variables.READ_EQUIPMENT_BY_SIZE;
            return Database.DataSetCommonPoint(procedureName, size);
        }

        public static DataSet readEquipmentByTeamId(string TEAMID)
        {
            string procedureName = Variables.READ_EQUIPMENT_BY_TEAMID;
            return Database.DataSetCommonPoint(procedureName, TEAMID);
        }

        public static DataSet getItemsByAthlete(string NYUID)
        {
            string procedureName = Variables.READ_EQUIPMENT_BY_ATHLETE;
            return Database.DataSetCommonPoint(procedureName, NYUID);
        }

        public static DataSet getIssuedItemsByNYUID(string NYUID)
        {
            string procedureName = Variables.READ_ISSUED_EQUIPMENT_BY_NYUID;
            return Database.DataSetCommonPoint(procedureName, NYUID);
        }

        public static int issueEquipment(string EQID, string NYUID, string qty, bool isUnNumbered)
        {
            string issueDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string procedureName = Variables.CREATE_EQUIPMENT_ATHLETE;
            int affectedRows = Database.IntegerCommonPoint(procedureName, EQID, NYUID, qty, issueDate);
            /*
                        if (isUnNumbered)
                        {
                            string name = Variables.UPDATE_EQUIPMENT_QUANTITY;
                            return Database.IntegerCommonPoint(name, EQID, NYUID, qty, issueDate);
                        }
             * */
            return affectedRows;
        }

        public static int removeIssuedEquipment(string EQID, string issuedDate, string qty, bool isUnNumbered)
        {
            string returnDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");//DateTime.Now.ToString("yyyyMMddHHmmssfff")
            string procedureName = Variables.REMOVE_ISSUED_EQUIPMENT;
            int affectedRows = Database.IntegerCommonPoint(procedureName, EQID, issuedDate, returnDate);
            if (affectedRows != -1)
            {
                string name = Variables.UPDATE_ISSUED_EQUIPMENT_QUANTITY;
                if (isUnNumbered)
                    return Database.IntegerCommonPoint(name, EQID, issuedDate, qty);
            }
            return affectedRows;
        }

        public static DataSet readEquipmentById(string id)
        {
            string procedureName = Variables.READ_ISSUED_EQUIPMENT_BY_EQID;
            return Database.DataSetCommonPoint(procedureName, id);
        }

        public static DataSet readEquipmentByAthleteWithSize(string size, string NYUID)
        {
            string procedureName = Variables.READ_EQUIPMENT_BY_ATHLETE_WITH_SIZE;
            return Database.DataSetCommonPoint(procedureName, size, NYUID);
        }

        public static int deleteEquipmentRecordFromIssue(string EQID, string NYUID, string date)
        {
            string procedureName = Variables.DELETE_EQUIPMENT_ISSUE_ATHLETE;
            return Database.IntegerCommonPoint(procedureName, EQID, NYUID, date);
        }

        public static int deleteAllEquipmentRecordFromIssue(string NYUID)
        {
            string procedureName = Variables.DELETE_ALL_EQUIPMENT_ISSUE_ATHLETE;
            return Database.IntegerCommonPoint(procedureName, NYUID);
        }
    }
}
