﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    class Database
    {
        private static MySqlConnection connection;
        private static MySqlConnection getConnection()
        {
            if (connection == null)
                connection = new MySqlConnection(CONNECTION_STRING);
            return connection;
        }

        private const string DATABASE_NAME = "nyudb";
        private const string SERVER = "localhost";
        private const string USER = "root";
        private const string PASSWORD = "";
        private static string CONNECTION_STRING = "SERVER=" + SERVER + ";" + "DATABASE=" + DATABASE_NAME + ";" + "UID=" + USER + ";" + "PASSWORD=" + PASSWORD + ";";

        public static DataSet DataSetCommonPoint(string ProcedureName, params string[] args)
        {
            MySqlCommand cmd = FormMySqlCommand(ProcedureName, args);
            if (cmd != null)
            {
                cmd.Prepare();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                DataSet ds = new DataSet();

                try
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);
                    adapter.Dispose();
                    cmd.Dispose();
                }
                catch (MySqlException e)
                {
                    MessageBox.Show(e.Message);
                }
                finally
                {
                    connection.Close();
                }
                return ds;
            }
            connection.Close();
            return null;
        }

        public static int IntegerCommonPoint(string ProcedureName, params string[] args)
        {
            MySqlCommand cmd = FormMySqlCommand(ProcedureName, args);

            if (cmd != null)
            {
                try
                {
                    int rowsAffected = cmd.ExecuteNonQuery();
                    connection.Close();
                    return rowsAffected;
                }

                catch (Exception e)
                {
                    MessageBox.Show("The record exists at many places! You cannot delete it right now!"); //return some error
                   // MessageBox.Show(e.ToString()); //return some error
                }
            }
            connection.Close();
            return -1;
        }

        public static List<List<string>> ListCommonPoint(string ProcedureName, params string[] args)
        {
            MySqlCommand cmd = FormMySqlCommand(ProcedureName, args);
            List<List<string>> objects = new List<List<string>>();
            if (cmd != null)
            {
                cmd.Prepare();

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        List<string> list = new List<string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                            list.Add(reader.GetString(i));

                        objects.Add(list);
                    }
                }

            }
            connection.Close();
            return objects;
        }

        public static int GetCountCommonPoint(string ProcedureName, params string[] args)
        {

            MySqlCommand cmd = FormMySqlCommand(ProcedureName, args);
            int count = 0;
            if (cmd != null)
            {
                cmd.Prepare();

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                }

            }
            connection.Close();
            return count;
        }

        private static string FormParamString(string[] args)
        {
            string ParamString = "";
            if (args != null)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    string temp = "@param" + i;
                    ParamString += temp + ",";
                    //cmd.Parameters.AddWithValue(temp, args[i]);
                }
                ParamString = ParamString.TrimEnd(',');
            }
            return ParamString;
        }

        private static string FormQueryString(string ProcedureName, string ParamString)
        {
            string queryString = "call " + ProcedureName + "(" + ParamString + ")";
            return queryString;
        }

        private static MySqlCommand FormMySqlCommand(string ProcedureName, string[] args)
        {
            MySqlCommand command = new MySqlCommand();
            connection = getConnection();
            try
            {
                connection.Open();
                if (connection.State == ConnectionState.Open)
                {
                    command.Connection = connection;
                    string ParamString = "";
                    if (args != null)
                    {
                        for (int i = 0; i < args.Length; i++)
                        {
                            string temp = "@param" + i;
                            ParamString += temp + ",";
                            command.Parameters.AddWithValue(temp, args[i]);
                        }
                        ParamString = ParamString.TrimEnd(',');
                    }
                    string query = FormQueryString(ProcedureName, ParamString);
                    command.CommandText = query;
                    return command;
                }

            }

            catch (Exception e)
            {
                MessageBox.Show("Messgae: Cannot connect to the server! Please turn on the server and try again.");
                System.Environment.Exit(-1);
            }

            return null;
        }

        public static List<string> ClassObjectListCommonPoint(string procedureName, params string[] args)
        {
            MySqlCommand cmd = FormMySqlCommand(procedureName, args);
            List<string> objects = new List<string>();
            if (cmd != null)
            {
                cmd.Prepare();

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        objects.Add(reader.GetString(0));
                        objects.Add(reader.GetString(1));
                    }
                }

            }
            connection.Close();
            return objects;
        }
    }
}
