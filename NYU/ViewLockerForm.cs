﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class LockerViewForm : Form
    {
        private string ID;
        private string BY_ID;
        private List<List<string>> listOfObjects;
        private static int index;
        private int totalCount;
        private string LOCKERID;

        public LockerViewForm()
        {

        }

        public LockerViewForm(string ID, string BY_ID)
        {
            InitializeComponent();
            this.ID = ID;
            this.BY_ID = BY_ID;

            initializeDefaults();
            getObjects();
            loadDetails();
        }

        private void initializeDefaults()
        {
            index = -1;
            totalCount = 0;
            LOCKERID = null;
        }

        private void getObjects()
        {
            ReadInterface read;
            if (BY_ID.Equals(Variables.BY_LOCKERID))
                read = new Locker();

            else
                read = new Person();

            listOfObjects = read.readById(ID);
            totalCount = listOfObjects.Count;
            if (totalCount == 0)
                initializeDefaults();
            else
                index = 0;

            setTotalCount();
            setCurrentCountField();
        }

        private void setTotalCount()
        {
            countfield.Text = "Total # locker(s) issued: " + totalCount;
        }

        private void loadDetails()
        {
            if (totalCount > 0)
                populateDetails(listOfObjects[index]);
            //setCurrentCountField(index);
        }

        private void populateDetails(List<string> objects)
        {

            if (objects != null)
            {
                nyuidtext.Text = objects[0];
                LOCKERID = objects[1];
                leveltext.Text = objects[2];
                numbertext.Text = objects[3];
                serialtext.Text = objects[4];
                combotext.Text = objects[5];
                sizetext.Text = objects[6];
                fortext.Text = objects[7];
                idatetext.Text = objects[9];
                edatetext.Text = objects[10];
                semestertext.Text = objects[11];
                issuedbytext.Text = objects[12];
                nametext.Text = objects[13] + " " + objects[14];
                emailtext.Text = objects[15];
                phonetext.Text = objects[16];
                gendertext.Text = objects[17];
                typetext.Text = objects[18];
            }
        }

        private void setCurrentCountField()
        {
            currentfield.Text = "Currently viewing " + (index + 1) + " of " + totalCount;
        }

        private void prevbtn_Click(object sender, EventArgs e)
        {
            if (index == -1)
                return;

            if (index != 0)
            {
                index--;
                setCurrentCountField();
                loadDetails();
            }

        }

        private void nextbtn_Click(object sender, EventArgs e)
        {
            if (index == -1)
                return;

            if (index != totalCount - 1)
            {
                index++;
                setCurrentCountField();
                loadDetails();
            }
        }

        private void removebtn_Click(object sender, EventArgs e)
        {
            if (LOCKERID != null)
            {
                DialogResult dialogResult = MessageBox.Show(Variables.DELETE_MESSAGE, Variables.DELETE_TITLE, MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    int rowsAffected = Locker.removePersonLockerByLockerId(LOCKERID);
                    if (rowsAffected == -1)
                        MessageBox.Show(Variables.FAILED_DELETE);
                    else
                    {
                        MessageBox.Show(Variables.SUCCESS_DELETE);
                        
                        resetFields();
                        initializeDefaults();
                        getObjects();
                        loadDetails();
                    }
                }
            }
        }

        private void resetFields()
        {
            nyuidtext.Text = "";
            LOCKERID = null;
            leveltext.Text = "";
            numbertext.Text = "";
            serialtext.Text = "";
            combotext.Text = "";
            sizetext.Text = "";
            fortext.Text = "";
            idatetext.Text = "";
            edatetext.Text = "";
            semestertext.Text = "";
            issuedbytext.Text = "";
            nametext.Text = "";
            emailtext.Text = "";
            phonetext.Text = "";
            gendertext.Text = "";
            typetext.Text = "";
        }
    }
}
