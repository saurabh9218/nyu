﻿namespace NYU
{
    partial class LockerEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.combo3text = new System.Windows.Forms.TextBox();
            this.combo2text = new System.Windows.Forms.TextBox();
            this.donebtn = new System.Windows.Forms.Button();
            this.sizetext = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.leveltext = new System.Windows.Forms.ComboBox();
            this.combo1text = new System.Windows.Forms.TextBox();
            this.serialtext = new System.Windows.Forms.TextBox();
            this.numbertext = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.fortext = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // combo3text
            // 
            this.combo3text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo3text.ForeColor = System.Drawing.Color.Black;
            this.combo3text.Location = new System.Drawing.Point(234, 228);
            this.combo3text.Name = "combo3text";
            this.combo3text.Size = new System.Drawing.Size(26, 23);
            this.combo3text.TabIndex = 36;
            // 
            // combo2text
            // 
            this.combo2text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo2text.ForeColor = System.Drawing.Color.Black;
            this.combo2text.Location = new System.Drawing.Point(202, 228);
            this.combo2text.Name = "combo2text";
            this.combo2text.Size = new System.Drawing.Size(26, 23);
            this.combo2text.TabIndex = 35;
            // 
            // donebtn
            // 
            this.donebtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donebtn.ForeColor = System.Drawing.Color.Black;
            this.donebtn.Location = new System.Drawing.Point(123, 315);
            this.donebtn.Name = "donebtn";
            this.donebtn.Size = new System.Drawing.Size(89, 33);
            this.donebtn.TabIndex = 32;
            this.donebtn.Text = "DONE";
            this.donebtn.UseVisualStyleBackColor = true;
            this.donebtn.Click += new System.EventHandler(this.donebtn_Click);
            // 
            // sizetext
            // 
            this.sizetext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sizetext.ForeColor = System.Drawing.Color.Black;
            this.sizetext.Location = new System.Drawing.Point(170, 262);
            this.sizetext.Name = "sizetext";
            this.sizetext.Size = new System.Drawing.Size(100, 23);
            this.sizetext.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(69, 266);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 30;
            this.label8.Text = "Locker Size:";
            // 
            // leveltext
            // 
            this.leveltext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.leveltext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leveltext.ForeColor = System.Drawing.Color.Black;
            this.leveltext.FormattingEnabled = true;
            this.leveltext.Items.AddRange(new object[] {
            "",
            "C-1",
            "C-2"});
            this.leveltext.Location = new System.Drawing.Point(170, 124);
            this.leveltext.Name = "leveltext";
            this.leveltext.Size = new System.Drawing.Size(51, 23);
            this.leveltext.TabIndex = 29;
            // 
            // combo1text
            // 
            this.combo1text.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo1text.ForeColor = System.Drawing.Color.Black;
            this.combo1text.Location = new System.Drawing.Point(170, 228);
            this.combo1text.Name = "combo1text";
            this.combo1text.Size = new System.Drawing.Size(26, 23);
            this.combo1text.TabIndex = 28;
            // 
            // serialtext
            // 
            this.serialtext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialtext.ForeColor = System.Drawing.Color.Black;
            this.serialtext.Location = new System.Drawing.Point(170, 198);
            this.serialtext.Name = "serialtext";
            this.serialtext.Size = new System.Drawing.Size(100, 23);
            this.serialtext.TabIndex = 27;
            // 
            // numbertext
            // 
            this.numbertext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numbertext.ForeColor = System.Drawing.Color.Black;
            this.numbertext.Location = new System.Drawing.Point(170, 165);
            this.numbertext.Name = "numbertext";
            this.numbertext.Size = new System.Drawing.Size(100, 23);
            this.numbertext.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(68, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 15);
            this.label4.TabIndex = 25;
            this.label4.Text = "Locker Combo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(68, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 24;
            this.label3.Text = "Locker Serial:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(68, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 15);
            this.label2.TabIndex = 23;
            this.label2.Text = "Locker Level:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Locker #:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(66, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(257, 26);
            this.label6.TabIndex = 37;
            this.label6.Text = "EDIT LOCKER INFORMATION";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(68, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 15);
            this.label7.TabIndex = 38;
            this.label7.Text = "Locker For:";
            // 
            // fortext
            // 
            this.fortext.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fortext.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fortext.ForeColor = System.Drawing.Color.Black;
            this.fortext.FormattingEnabled = true;
            this.fortext.Items.AddRange(new object[] {
            "",
            "ATHLETE",
            "PATRON",
            "COACH",
            "STAFF"});
            this.fortext.Location = new System.Drawing.Point(171, 84);
            this.fortext.Name = "fortext";
            this.fortext.Size = new System.Drawing.Size(121, 23);
            this.fortext.TabIndex = 39;
            // 
            // LockerEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(366, 380);
            this.Controls.Add(this.fortext);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.combo3text);
            this.Controls.Add(this.combo2text);
            this.Controls.Add(this.donebtn);
            this.Controls.Add(this.sizetext);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.leveltext);
            this.Controls.Add(this.combo1text);
            this.Controls.Add(this.serialtext);
            this.Controls.Add(this.numbertext);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "LockerEditForm";
            this.Text = "LockerEditForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox combo3text;
        private System.Windows.Forms.TextBox combo2text;
        private System.Windows.Forms.Button donebtn;
        private System.Windows.Forms.TextBox sizetext;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox leveltext;
        private System.Windows.Forms.TextBox combo1text;
        private System.Windows.Forms.TextBox serialtext;
        private System.Windows.Forms.TextBox numbertext;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox fortext;
    }
}