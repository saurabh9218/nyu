﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class TeamList : Form
    {
        private static DataSet ds;
        public TeamList()
        {
            InitializeComponent();
            LoadTeams();
        }

        private void LoadTeams()
        {
            ds = Team.getAllTeams();
            if (ds != null)
            {
                teamslist.DataSource = ds.Tables[0];
                teamslist.Columns[0].Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            TeamCreateForm form = new TeamCreateForm(Variables.ADD);
            form.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            string TEAMID = teamslist.CurrentRow.Cells[0].Value.ToString();
            string name = teamslist.CurrentRow.Cells[1].Value.ToString();
            string gender = teamslist.CurrentRow.Cells[2].Value.ToString();
            string coach = teamslist.CurrentRow.Cells[3].Value.ToString();
            Team team = new Team(TEAMID, name, gender, coach);
            //ds.Tables[0].Rows[teamslist.CurrentRow];
            TeamCreateForm form = new TeamCreateForm(team);

            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string TEAMID = teamslist.CurrentRow.Cells[0].Value.ToString();
            Team team = new Team(TEAMID, null, null, null);
            int affectedRows = team.delete();
            if (affectedRows == -1)
                MessageBox.Show(Variables.FAILED_DELETE);
            else
                MessageBox.Show(Variables.SUCCESS_DELETE);
            LoadTeams();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            LoadTeams();
        }
    }
}
