﻿namespace NYU
{
    partial class PersonCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.personlist = new System.Windows.Forms.DataGridView();
            this.deletebtn = new System.Windows.Forms.Button();
            this.issueeqbtn = new System.Windows.Forms.Button();
            this.issuelockerbtn = new System.Windows.Forms.Button();
            this.editbtn = new System.Windows.Forms.Button();
            this.viewlockerbtn = new System.Windows.Forms.Button();
            this.addbtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.searchtxt = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.personlist)).BeginInit();
            this.SuspendLayout();
            // 
            // personlist
            // 
            this.personlist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.personlist.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.personlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.personlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.personlist.DefaultCellStyle = dataGridViewCellStyle2;
            this.personlist.Location = new System.Drawing.Point(22, 77);
            this.personlist.Name = "personlist";
            this.personlist.ReadOnly = true;
            this.personlist.RowTemplate.ReadOnly = true;
            this.personlist.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.personlist.Size = new System.Drawing.Size(1052, 442);
            this.personlist.TabIndex = 0;
            // 
            // deletebtn
            // 
            this.deletebtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletebtn.Location = new System.Drawing.Point(962, 44);
            this.deletebtn.Name = "deletebtn";
            this.deletebtn.Size = new System.Drawing.Size(110, 31);
            this.deletebtn.TabIndex = 8;
            this.deletebtn.Text = "DELETE PATRON";
            this.deletebtn.UseVisualStyleBackColor = true;
            this.deletebtn.Click += new System.EventHandler(this.deletebtn_Click);
            // 
            // issueeqbtn
            // 
            this.issueeqbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.issueeqbtn.Location = new System.Drawing.Point(22, 40);
            this.issueeqbtn.Name = "issueeqbtn";
            this.issueeqbtn.Size = new System.Drawing.Size(131, 34);
            this.issueeqbtn.TabIndex = 7;
            this.issueeqbtn.Text = "ATHLETE CARD";
            this.issueeqbtn.UseVisualStyleBackColor = true;
            this.issueeqbtn.Click += new System.EventHandler(this.issueeqbtn_Click);
            // 
            // issuelockerbtn
            // 
            this.issuelockerbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.issuelockerbtn.Location = new System.Drawing.Point(856, 44);
            this.issuelockerbtn.Name = "issuelockerbtn";
            this.issuelockerbtn.Size = new System.Drawing.Size(100, 31);
            this.issuelockerbtn.TabIndex = 6;
            this.issuelockerbtn.Text = "ISSUE LOCKER";
            this.issuelockerbtn.UseVisualStyleBackColor = true;
            this.issuelockerbtn.Click += new System.EventHandler(this.issuelockerbtn_Click);
            // 
            // editbtn
            // 
            this.editbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editbtn.Location = new System.Drawing.Point(962, 12);
            this.editbtn.Name = "editbtn";
            this.editbtn.Size = new System.Drawing.Size(110, 30);
            this.editbtn.TabIndex = 5;
            this.editbtn.Text = "EDIT PATRON";
            this.editbtn.UseVisualStyleBackColor = true;
            this.editbtn.Click += new System.EventHandler(this.editbtn_Click);
            // 
            // viewlockerbtn
            // 
            this.viewlockerbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewlockerbtn.Location = new System.Drawing.Point(856, 12);
            this.viewlockerbtn.Name = "viewlockerbtn";
            this.viewlockerbtn.Size = new System.Drawing.Size(100, 30);
            this.viewlockerbtn.TabIndex = 9;
            this.viewlockerbtn.Text = "VIEW LOCKER";
            this.viewlockerbtn.UseVisualStyleBackColor = true;
            this.viewlockerbtn.Click += new System.EventHandler(this.viewlockerbtn_Click);
            // 
            // addbtn
            // 
            this.addbtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addbtn.Location = new System.Drawing.Point(22, 4);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(131, 31);
            this.addbtn.TabIndex = 10;
            this.addbtn.Text = "ADD";
            this.addbtn.UseVisualStyleBackColor = true;
            this.addbtn.Click += new System.EventHandler(this.addbtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(252, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 15);
            this.label1.TabIndex = 11;
            this.label1.Text = "Search by name or team:";
            // 
            // searchtxt
            // 
            this.searchtxt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchtxt.Location = new System.Drawing.Point(400, 48);
            this.searchtxt.Name = "searchtxt";
            this.searchtxt.Size = new System.Drawing.Size(175, 23);
            this.searchtxt.TabIndex = 12;
            this.searchtxt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(775, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 30);
            this.button1.TabIndex = 13;
            this.button1.Text = "REFRESH";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PersonCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(46)))), ((int)(((byte)(145)))));
            this.ClientSize = new System.Drawing.Size(1099, 531);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.searchtxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addbtn);
            this.Controls.Add(this.viewlockerbtn);
            this.Controls.Add(this.deletebtn);
            this.Controls.Add(this.issueeqbtn);
            this.Controls.Add(this.issuelockerbtn);
            this.Controls.Add(this.editbtn);
            this.Controls.Add(this.personlist);
            this.Name = "PersonCenter";
            this.Text = "PersonCenter";
            ((System.ComponentModel.ISupportInitialize)(this.personlist)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView personlist;
        private System.Windows.Forms.Button deletebtn;
        private System.Windows.Forms.Button issueeqbtn;
        private System.Windows.Forms.Button issuelockerbtn;
        private System.Windows.Forms.Button editbtn;
        private System.Windows.Forms.Button viewlockerbtn;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchtxt;
        private System.Windows.Forms.Button button1;
    }
}