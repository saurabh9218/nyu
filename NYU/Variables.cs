﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYU
{
    class Variables
    {
        public const string CONTACT_ADMIN = "Please contact System Admin!";

        //CREATE STRINGS
        public const string CREATE_PERSON_PROCEDURE = "createPerson";
        public const string CREATE_ATHLETE_PROCEDURE = "createAthlete";
        public const string CREATE_LOCKER_PROCEDURE = "createLocker";
        public const string CREATE_PERSON_LOCKER_PROCEDURE = "createPersonLocker";
        public const string CREATE_PERSON_TEAM_PROCEDURE = "createPersonTeam";
        public const string CREATE_TEAM_LOCKER_PROCEDURE = "createTeamLocker";
        public const string CREATE_TEAM_PROCEDURE = "createTeam";
        public const string CREATE_EQUIPMENT_PROCEDURE = "createEquipment";
        public static string CREATE_EQUIPMENT_ATHLETE = "createEquipmentIssue";
        
        //READ STRINGS
        public const string READ_PERSON_PROCEDURE = "readPerson";
        public const string READ_ATHLETE_PROCEDURE = "readAthlete";
        public const string READ_LOCKER_PROCEDURE = "readLocker";
        public const string READ_ATHLETE_LOCKER_PROCEDURE = "readAthleteLocker";
        public const string READ_BY_ID_LOCKER_PROCEDURE = "readLockerById";
        public const string READ_BY_ID_PERSON_PROCEDURE = "readPersonById";
        public const string READ_PERSON_BY_TYPE_PROCEDURE = "readPersonByType";
        public const string READ_TEAM_BY_ID = "readTeamById";
        public const string READ_TEAMS = "readTeams";
        public const string READ_ALL_TEAMS_PROCEDURE = "readAllTeams";
        public const string READ_EQUIPMENT_PROCEDURE = "readEquipment";
        public const string READ_EQUIPMENT_BY_SIZE = "EquipmentViewBySize";
        public const string READ_EQUIPMENT_BY_TEAMID = "EquipmentViewByTeamId";
        public const string READ_EQUIPMENT_BY_ATHLETE = "readEquipmentByAthlete";
        public const string READ_ISSUED_EQUIPMENT_BY_NYUID = "readIssuedEquipmentByNYUID";
        public const string READ_ISSUED_EQUIPMENT_BY_EQID = "readEquipmentByEQID";
        public const string READ_EQUIPMENT_BY_ATHLETE_WITH_SIZE = "readEquipmentByAthleteWithSize";
       
        //UPDATE STRINGS
        public const string UPDATE_PERSON_PROCEDURE = "updatePerson";
        public const string UPDATE_LOCKER_PROCEDURE = "updateLocker";
        public const string UPDATE_TEAM_LOCKER_PROCEDURE = "updateTeamLocker";
        public const string UPDATE_TEAM_PROCEDURE = "updateTeam";
        public const string UPDATE_EQUIPMENT_PROCEDURE = "updateEquipment";
        public const string UPDATE_LOCKER_AVAILABILITY_PROCEDURE = "updateLockerAvailability";
        public const string UPDATE_ISSUED_EQUIPMENT_QUANTITY = "updateIssuedEquipmentQuantity";
        
        //REMOVE STRINGS
        public const string REMOVE_LOCKER_PROCEDURE = "removeLocker";
        public const string REMOVE_PERSON_PROCEDURE = "removePerson";
        public const string REMOVE_PERSON_LOCKER_PROCEDURE = "removePersonLockerById";
        public const string DELETE_TEAM_PROCEDURE = "deleteTeam";
        public const string REMOVE_EQUIPMENT_PROCEDURE = "removeEquipment";
        public const string REMOVE_ISSUED_EQUIPMENT = "deleteIssuedEquipment"; // changing return date

        //EXISTS STRINGS
        public const string EXISTS_PERSON_RECORD = "personExists";

        //MESSAGE BOX STRINGS
        public const string FILL_DETAILS = "Please fill all the details and submit!";

        public const string CREATE_FAILED = "Failed to create! " + CONTACT_ADMIN;
        public const string CREATE_SUCCESS = "Create Success! The record is added into the system!";

        public const string UPDATE_FAILED = "Failed to update! " + CONTACT_ADMIN;
        public const string UPDATE_SUCCESS = "Update Success! Update is made into the system!";

        public const string FAILED_DELETE = "Failed to delete! " + CONTACT_ADMIN;
        public const string SUCCESS_DELETE = "Delete Success! The record has been deleted!";

     
        public const string LOCKER_EXISTS = "Locker exists!";

      //  public const string CREATE_NEW_LOCKER_FAILED = "Failed to add a new locker! " + CONTACT_ADMIN;
      //  public const string CREATE_NEW_LOCKER_SUCCESS = "Success! Locker added!";

        //DATABASE FIELD STRINGS
        public const string PERSON = "PERSON";
        public const string ATHLETE = "ATHLETE";
        public const string PATRON = "PATRON";
        public const string DAILY = "DAILY";
        public const string ALL = "%";
        public const string COACH = "COACH";
        public const string NUMBERED = "Numbered";
        public const string UN_NUMBERED = "Un-Numbered";
        public const string TEAM = "Team";
        public const string SIZE = "Size";

        public const string YES = "YES";
        public const string NO = "NO";
        public const string ADD = "ADD";
        public const string EDIT = "EDIT";
       

        public const string UNAVAILABLE = "Sorry! The selected item is not available!";
        public const string DELETE_MESSAGE = "Are you Sure, you want to delete this Record?";
        public const string DELETE_TITLE = "Confirm Delete?";
        public const string LOCKER_ALERT = "Locker Alert!";
        public const string CANNOT_DELETE_LOCKER = "Sorry! You cannot delete this locker since it is issued to someone. Use the Athlete/Patron center!";
        public const string BY_LOCKERID = "LOCKERID";
        public const string BY_PERSONID = "PERSONID";
        public const string CONTINUE = "Do you want to proceed?";
        public const string DELETE_EQUIPMENT_ISSUE_ATHLETE = "deleteEquipmentFromAthleteIssue";
        public const string DELETE_ALL_EQUIPMENT_ISSUE_ATHLETE = "deleteAllEquipmentFromAthleteIssue";
        public const string WRONG_EMAIL= "Please check the email!";
        public const string READ_PERSON_SEARCH_PROCEDURE = "searchPerson";
        public const string READ_ATHLETE_SEARCH_PROCEDURE = "searchAthlete";
    }
}
