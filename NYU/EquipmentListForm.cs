﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class EquipmentListForm : Form
    {
        private static DataSet ds;
        private List<Team> teamList;
        private static int count;
        public EquipmentListForm()
        {
            InitializeComponent();
            LoadEquipments();
        }

        private void LoadEquipments()
        {
            ds = Equipment.read();
            setDs();
        }

        private void setDs()
        {
            if (ds != null)
            {
               
                eqlist.DataSource = ds.Tables[0];
                count = eqlist.RowCount;
                eqlist.Columns[0].Visible = false;
                eqlist.Columns[1].Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
            EquipmentForm form = new EquipmentForm(Variables.ADD);
            form.Show();
        }

        private void editbtn_Click(object sender, EventArgs e)
        {
            //private string EQID, TEAMID, Equipment_Item, Type, Qty_Number, Style, Gender, Size, Color, Price, Date, ReturnedStatus;
            //int count = eqlist.RowCount;
            if (count > 1)
            {
                String EQID = eqlist.CurrentRow.Cells[0].Value.ToString();
                String TEAMID = eqlist.CurrentRow.Cells[1].Value.ToString();
                String TeamName = eqlist.CurrentRow.Cells[2].Value.ToString();
                String ItemName = eqlist.CurrentRow.Cells[3].Value.ToString();
                String Type = eqlist.CurrentRow.Cells[4].Value.ToString();
                String Qty_Number = eqlist.CurrentRow.Cells[5].Value.ToString();
                String Style = eqlist.CurrentRow.Cells[6].Value.ToString();
                String Gender = eqlist.CurrentRow.Cells[7].Value.ToString();
                String Size = eqlist.CurrentRow.Cells[8].Value.ToString();
                String Color = eqlist.CurrentRow.Cells[9].Value.ToString();
                String Price = eqlist.CurrentRow.Cells[10].Value.ToString();
                String Date = eqlist.CurrentRow.Cells[11].Value.ToString();
                String Status = eqlist.CurrentRow.Cells[12].Value.ToString();

                Equipment equipment = new Equipment(EQID, TEAMID, ItemName, Type, Qty_Number, Style, Gender, Size, Color, Price, Date, Status);

                base.OnClick(e);
                EquipmentForm form = new EquipmentForm(equipment);
                form.Show();
            }
        }

        private void refreshbtn_Click(object sender, EventArgs e)
        {
            LoadEquipments();
        }

        private void deletebtn_Click(object sender, EventArgs e)
        {
           // int count = eqlist.Rows.Count;
            if (count > 1)
            {
                DialogResult dialogResult = MessageBox.Show(Variables.DELETE_MESSAGE, Variables.DELETE_TITLE, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    String EQID = eqlist.CurrentRow.Cells[0].Value.ToString();
                    int rowsAffected = Equipment.remove(EQID);
                    if (rowsAffected == -1)
                        MessageBox.Show(Variables.FAILED_DELETE);
                    else
                    {
                        MessageBox.Show(Variables.SUCCESS_DELETE);
                        LoadEquipments();
                    }
                }

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string option = viewbytext.Text;
            switch (option)
            {
                case Variables.TEAM:
                    viewByTeam();
                    break;
                case Variables.SIZE:
                    viewBySize();
                    break;
            }
        }

        private void viewBySize()
        {
            optiontext.DataSource = null;
            optiontext.Items.Add("");
            optiontext.Items.Add("XY");
            optiontext.Items.Add("XS");
            optiontext.Items.Add("SM");
            optiontext.Items.Add("MD");
            optiontext.Items.Add("LG");
            optiontext.Items.Add("XL");
            optiontext.Items.Add("XXL");
            optiontext.Items.Add("XXXL");
        }

        private void viewByTeam()
        {
            LoadTeams();
        }

        private void LoadTeams()
        {
            teamList = Team.getTeams();
            optiontext.DataSource = teamList;
            optiontext.ValueMember = "TEAMID";
            optiontext.DisplayMember = "Name";

        }

        private void optiontext_SelectedIndexChanged(object sender, EventArgs e)
        {
            string viewByText = viewbytext.Text;
            string option = optiontext.Text;
            if (viewByText.Equals(Variables.SIZE))
                ds = Equipment.readEquipmentBySize(option);

            else if (viewByText.Equals(Variables.TEAM))
            {
                int index = optiontext.SelectedIndex;
                string TEAMID = teamList[index].TEAMID;
                ds = Equipment.readEquipmentByTeamId(TEAMID);
            }
            setDs();
        }

        private void viewinfobtn_Click(object sender, EventArgs e)
        {
            if (count > 1)
            {
                base.OnClick(e);
                String EQID = eqlist.CurrentRow.Cells[0].Value.ToString();
                ViewEquipmentForm form = new ViewEquipmentForm(EQID);
                form.Show();
            }
        }
    }
}
