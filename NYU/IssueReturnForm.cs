﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class IssueReturnForm : Form
    {
        private static string NYUID;
        private static DataSet itemListDs;
        private static DataSet issueListDs;
        private static Person person;
        private static int count;

        public IssueReturnForm(Person p)
        {
            InitializeComponent();
            NYUID = p.getNYUID();
            person = p;
            LoadAthleteDetails();
            refresh();
        }

        void refresh()
        {
            LoadItemList();
            LoadIssuedItems();
            qtynumtext.Value = 0;
        }


        private void LoadAthleteDetails()
        {
            nametext.Text = person.getFirstName() + " " + person.getLastName();
            emailtxt.Text = person.getEmail();
            phonetxt.Text = person.getPhone();
            nyuidtext.Text = NYUID;
        }

        private void LoadIssuedItems()
        {
            issueListDs = Equipment.getIssuedItemsByNYUID(NYUID);
            setIssuedList();
        }

        private void setIssuedList()
        {
            if (issueListDs != null)
            {
                issuelist.DataSource = issueListDs.Tables[0];
                count = issuelist.Rows.Count;
                issuelist.Columns[0].Visible = false;
                issuelist.Columns[1].Visible = false;
                issuelist.Columns[2].Visible = false;
                issuelist.Columns[4].Visible = false;
            }
        }

        private void LoadItemList()
        {
            itemListDs = Equipment.getItemsByAthlete(NYUID);
            setItemList();
        }

        private void setItemList()
        {
            if (itemListDs != null)
            {
                itemlist.DataSource = itemListDs.Tables[0];
                itemlist.Columns[0].Visible = false;
                itemlist.Columns[1].Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string type = itemlist.CurrentRow.Cells[3].Value.ToString();
            string qty = "1";
            bool isUnNumbered = false;
            if (type.Equals(Variables.UN_NUMBERED))
            {
                isUnNumbered = true;
                if (qtynumtext.Value < 1)
                {
                    MessageBox.Show("Please enter the Quantity");
                    return;
                }
                qty = qtynumtext.Value.ToString();

            }

            string EQID = itemlist.CurrentRow.Cells[0].Value.ToString();
            int affectedRows = Equipment.issueEquipment(EQID, NYUID, qty, isUnNumbered);
            if (affectedRows == -1)
                MessageBox.Show(Variables.CREATE_FAILED);
            else
            {
                MessageBox.Show(Variables.CREATE_SUCCESS);
                refresh();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //SELECT a.NYUID, x.EQID, a.TEAMID, e.Equipment_Item, e.type, e.Size, e.Color, case e.Type WHEN 'Numbered' then e.Qty_Number
            //else x.Qty_Number
            //END as Qty_Number, 
            //x.IssueDate, x.ReturnDate
            string EQID = issuelist.CurrentRow.Cells[1].Value.ToString();
            string type = issuelist.CurrentRow.Cells[4].Value.ToString();
            string issuedQty = issuelist.CurrentRow.Cells[7].Value.ToString();
            int returnedQty = (int)qtynumtext.Value;
            bool isUnNumbered = false;
            if (type.Equals(Variables.UN_NUMBERED))
            {
                isUnNumbered = true;
                int iQty = Convert.ToInt32(issuedQty);
                if (returnedQty < 1 || returnedQty > iQty)
                {
                    MessageBox.Show("Please enter a correct quantity value that is greater than 0 and less than or equal to " + iQty);
                    return;
                }
            }
            string issuedDate = issuelist.CurrentRow.Cells[8].Value.ToString();
            int affectedRows = Equipment.removeIssuedEquipment(EQID, issuedDate, returnedQty.ToString(), isUnNumbered); // this means update return date

            if (affectedRows == -1)
                MessageBox.Show(Variables.UPDATE_FAILED);
            else
            {
                MessageBox.Show(Variables.UPDATE_SUCCESS);
                refresh();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            refresh();
        }


        private void sizetext_SelectedIndexChanged(object sender, EventArgs e)
        {
            string size = sizetext.Text.ToString();
            if (String.IsNullOrEmpty(size))
                size = "%";
            itemListDs = Equipment.readEquipmentByAthleteWithSize(size, NYUID);
            setItemList();
        }

        private void deletebtn_Click(object sender, EventArgs e)
        {
            if (count > 1)
            {
                string NYUID = issuelist.CurrentRow.Cells[0].Value.ToString();
                string EQID = issuelist.CurrentRow.Cells[1].Value.ToString();
                string issuedDate = issuelist.CurrentRow.Cells[8].Value.ToString();
                string returnDate = issuelist.CurrentRow.Cells[9].Value.ToString();
                int affectedRows = -1;

                if (!String.IsNullOrEmpty(returnDate))
                {
                    affectedRows = Equipment.deleteEquipmentRecordFromIssue(EQID, NYUID, issuedDate);

                    if (affectedRows == -1)
                        MessageBox.Show(Variables.FAILED_DELETE);
                    else
                        MessageBox.Show(Variables.SUCCESS_DELETE);
                }

                else
                    MessageBox.Show("Cannot delete this item since it is issued");
            }
        }

        private void deleteallbtn_Click(object sender, EventArgs e)
        {
            if (count > 1)
            {
                string NYUID = issuelist.CurrentRow.Cells[0].Value.ToString();
                int affectedRows = affectedRows = Equipment.deleteAllEquipmentRecordFromIssue(NYUID);

                if (affectedRows == -1)
                    MessageBox.Show(Variables.FAILED_DELETE);
                else
                    MessageBox.Show(Variables.SUCCESS_DELETE);
            }
        }
    }
}
