﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYU
{
    public partial class LockerEditForm : Form
    {
        private Locker locker;

        public LockerEditForm(Locker locker)
        {
            InitializeComponent();
            this.locker = locker;
            LoadFormDetails();
        }

        private void LoadFormDetails()
        {
            leveltext.Text = locker.getLevel();
            numbertext.Text = locker.getNumber();
            serialtext.Text = locker.getSerial();
            string[] combo = locker.getCombo().Split('-');
            combo1text.Text = combo[0];
            combo2text.Text = combo[1];
            combo3text.Text = combo[2];
            sizetext.Text = locker.getSize();
            fortext.Text = locker.getLockerFor();
        }

        private void donebtn_Click(object sender, EventArgs e)
        {
            List<string> parameters = new List<string>();
          
            parameters.Add(leveltext.Text);
            parameters.Add(numbertext.Text);
            parameters.Add(serialtext.Text);
            parameters.Add(combo1text.Text);
            parameters.Add(combo2text.Text);
            parameters.Add(combo3text.Text);
            parameters.Add(sizetext.Text);
            parameters.Add(fortext.Text);

            if (Helper.isBlank(parameters))
                MessageBox.Show(Variables.FILL_DETAILS);
            else
            {
                string combo = parameters[3] + "-" + parameters[4] + "-" + parameters[5];
                string LOCKERID = this.locker.getLockerId();
                Locker locker = new Locker(LOCKERID, parameters[0], parameters[1], parameters[2], combo, parameters[6], parameters[7], "",null);
                
                int rowsAffected = locker.update();
                
                if (rowsAffected != 1)
                    MessageBox.Show(Variables.UPDATE_FAILED);
                else
                {
                    MessageBox.Show(Variables.UPDATE_SUCCESS);
                    this.Close();
                }
            }
        }
    }
}
